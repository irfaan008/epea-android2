package com.expenses.epea;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPScreen extends BaseActivity {


    @BindView(R.id.dot_1)
    TextView dot1;
    @BindView(R.id.dot_2)
    TextView dot2;
    @BindView(R.id.dot_3)
    TextView dot3;
    @BindView(R.id.dot_4)
    TextView dot4;
    @BindView(R.id.dot_5)
    TextView dot5;
    @BindView(R.id.dot_6)
    TextView dot6;
    @BindView(R.id.tap_1)
    TextView tap1;
    @BindView(R.id.tap_2)
    TextView tap2;
    @BindView(R.id.tap_3)
    TextView tap3;
    @BindView(R.id.tap_4)
    TextView tap4;
    @BindView(R.id.tap_5)
    TextView tap5;
    @BindView(R.id.tap_6)
    TextView tap6;
    @BindView(R.id.tap_7)
    TextView tap7;
    @BindView(R.id.tap_8)
    TextView tap8;
    @BindView(R.id.tap_9)
    TextView tap9;
    @BindView(R.id.tap_0)
    TextView tap0;
    @BindView(R.id.tap_back)
    ImageView tapBack;

    ArrayList<String> arr = new ArrayList<String>();
    ProgressDialog pd;
    String SERVER_URL;
    String email = "";
    SharedPreferencesKV sharedPreferencesKV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        ButterKnife.bind(this);

        sharedPreferencesKV = new SharedPreferencesKV(OTPScreen.this);

        if(getIntent().getExtras() != null){
            email = getIntent().getStringExtra("email");
        }

    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick({R.id.tap_1, R.id.tap_2, R.id.tap_3, R.id.tap_4, R.id.tap_5, R.id.tap_6, R.id.tap_7, R.id.tap_8, R.id.tap_9, R.id.tap_0, R.id.tap_back})
    public void onViewClicked(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.tap_1:
                addDigit("1");
                break;
            case R.id.tap_2:
                addDigit("2");
                break;
            case R.id.tap_3:
                addDigit("3");
                break;
            case R.id.tap_4:
                addDigit("4");
                break;
            case R.id.tap_5:
                addDigit("5");
                break;
            case R.id.tap_6:
                addDigit("6");
                break;
            case R.id.tap_7:
                addDigit("7");
                break;
            case R.id.tap_8:
                addDigit("8");
                break;
            case R.id.tap_9:
                addDigit("9");
                break;
            case R.id.tap_0:
                addDigit("0");
                break;
            case R.id.tap_back:
                if(arr.size()>0){
                    removeDigit();
                }

                break;
        }
    }

    public String addDigit(String tapDigit){

        arr.add(tapDigit);
        if(arr.size() == 1){
            dot1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);

        }else if(arr.size() == 2){
            dot2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);

        }else if(arr.size() == 3){
            dot3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);

        }else if(arr.size() == 4){
            dot4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);

        }else if(arr.size() == 5){
            dot5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);

        }else if(arr.size() == 6){
            dot6.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_selected, 0);


            StringBuilder builder = new StringBuilder();
            for(int i=0; i<arr.size(); i++){
                builder.append(arr.get(i));
                //builder.append(arr.get(i)+",");
            }
            //String otpStr = builder.toString().substring(0, builder.toString().length() - 1);
            //Toast.makeText(this, ""+builder.toString(), Toast.LENGTH_SHORT).show();
            sendOTPRequest(OTPScreen.this,builder.toString());
            /*Intent i = new Intent(OTPScreen.this,ClientServices.class);
            startActivity(i);*/
        }

        //Toast.makeText(this, ""+arr.toString(), Toast.LENGTH_SHORT).show();

        return null;
    }

    public String removeDigit(){

        if(arr.size() == 6){
            dot6.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }else if(arr.size() == 5){
            dot5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }else if(arr.size() == 4){
            dot4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }else if(arr.size() == 3){
            dot3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }else if(arr.size() == 2){
            dot2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }else if(arr.size() == 1){
            dot1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.circle_unseleted, 0);

        }
        arr.remove(arr.size()-1);
        //Toast.makeText(this, ""+arr.toString(), Toast.LENGTH_SHORT).show();
        return null;
    }

    private void sendOTPRequest(Context ctx,String mOTP) {

        pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("otp", mOTP);
        //params.put("client_id", "");

        SERVER_URL = Constant.SERVER_URL+"api/auth/otp";

        Log.e("request  ","request server url:-    "+SERVER_URL+"    "+params);
        sendServerRequest(ctx,SERVER_URL, params, VolleyResponseListener.SEND_OTP);


    }

    private void sendServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {
        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(tapBack,"No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ","response  "+response.toString());
                if(responseId == VolleyResponseListener.SEND_OTP) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if(jsonRootObject.getInt("status") == 200){
                            //Toast.makeText(ctx, response.toString(), Toast.LENGTH_SHORT).show();
                            JSONObject OBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);
                            sharedPreferencesKV.storeLoginData(OBJ.getString("user_id"),
                                    OBJ.getString("client_id"),OBJ.getString("name"),
                                    OBJ.getString("expense"),OBJ.getString("logo"),
                                    OBJ.getString("approval"),OBJ.getString("currency"),
                                    OBJ.getBoolean("expense_project"),OBJ.getBoolean("expense_notes"),
                                    OBJ.getBoolean("expense_booking_no"));
                            Toast.makeText(ctx, jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(OTPScreen.this,ClientServices.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }else{
                            Toast.makeText(ctx, ""+ Utils.statusMsg(ctx,jsonRootObject.getInt("status"),jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ctx, "Something went wrong ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
