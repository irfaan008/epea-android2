package com.expenses.epea.remote;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ankit on 9/18/2018.
 */

public class VolleyUtils {

    private static Map<String, String> postParams;
    public static int MY_SOCKET_TIMEOUT_MS = 180000;
    private static final String FILE_PART_NAME = "file";
    private static final String STRING_PART_NAME = "text";


    public static void makePostReq(Context context, int method,  final Map<String, String> params, String url, final int rid, final VolleyResponseListener listener) {
        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(response,rid);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                volleyError = error;*/
                listener.onError(volleyReturnError(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("Accept", "application/json");
                header.put("Content-Type", "application/x-www-form-urlencoded");
                /*header.put("Content-Type", "application/x-www-form-urlencoded");
                header.put("Authorization", "Basic YWRtaW46ZWlqb2hyZUwzQWh4ZWVwaDJpb2Q=");*/

                return header;
            }

            @Override
            protected Map<String, String> getParams()
            {
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    //return Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
                    return Response.success(String.valueOf(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                }catch (Exception e){
                    return Response.error(new ParseError(e));
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through singleton class.
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void makeJsonGETReq(Context context, int method,  String url, final int rid, final VolleyResponseListener listener) {
        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(response,rid);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(volleyReturnError(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("Accept", "application/json");
                header.put("Authorization", "Basic YWRtaW46ZWlqb2hyZUwzQWh4ZWVwaDJpb2Q=");
                return header;
            }


            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    //return Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
                    return Response.success(String.valueOf(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                }catch (Exception e){
                    return Response.error(new ParseError(e));
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through singleton class.
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void multipartImageRequest(final Context context, String url, final Map<String, String> params, final Map<String, VolleyMultipartRequest.DataPart> imagesBitmap, final int rid, final VolleyResponseListener listener){

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.e("date check  ","check  response   "+new String(response.data));
                            //Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            listener.onResponse(obj,rid);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                        listener.onError(error.toString());
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                /*Map<String, String> params = new HashMap<>();
                params.put("tags", "test for uploading");*/
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {

                return imagesBitmap;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("Accept", "application/json");
                //header.put("Content-Type", "application/json");
                //header.put("Content-Type", "application/x-www-form-urlencoded");
                //header.put("Authorization", "Basic YWRtaW46ZWlqb2hyZUwzQWh4ZWVwaDJpb2Q=");

                return header;
            }
        };

        //adding the request to volley
        Volley.newRequestQueue(context).add(volleyMultipartRequest);

    }



    public static String volleyReturnError(VolleyError error){
        String message = "";
        if( error instanceof NetworkError) {
            message = "Network error";
        }  else if( error instanceof ServerError) {
            message = "Server error";
        } else if( error instanceof AuthFailureError) {
            message = "AuthFailureError error";
        } else if( error instanceof ParseError) {
            message = "ParseError Found";
        } else if( error instanceof NoConnectionError) {
            message = "No internet connection";
        } else if( error instanceof TimeoutError) {
            message = "Timeout, Please try again!";
        }else{
            message = "Server not responding";
        }

        return message;
    }
}
