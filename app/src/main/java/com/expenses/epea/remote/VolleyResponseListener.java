package com.expenses.epea.remote;

/**
 * Created by Ankit on 9/18/2018.
 */

public interface VolleyResponseListener {

    public static final int VERIFY_LOGIN = 1;
    public static final int LOGIN = 2;
    public static final int FORGET_PASSWORD = 3;
    public static final int GET_CATEGORIES = 4;
    public static final int GET_PROJECTS = 5;
    public static final int GET_CURRENCIES = 6;
    public static final int ADD_EXPENSES = 7;
    public static final int ALL_DRAFT_EXPENSES = 8;
    public static final int APPROVE_REPORT = 9;
    public static final int SEND_OTP = 10;
    public static final int REPORT_EXPENSES = 11;
    public static final int APPROVAL_EXPENSES_LIST = 12;
    public static final int EXPENSE_REFRENCE_LIST = 13;
    public static final int EXPENSE_DETAIL = 14;
    public static final int APPROVE_SINGLE_EXPENSE = 15;
    public static final int REJECT_SINGLE_EXPENSE = 16;
    public static final int USER_PROFILE = 17;
    public static final int DELETE_DRAFTS_EXPENSE = 18;
    public static final int EDIT_DRAFTS_EXPENSE = 19;
    public static final int EXPENSE_CLONE = 20;
    public static final int CHANGE_PASSWORD = 21;
    public static final int CURRENCY_EXCHANGE = 22;
    public static final int VIEW_DRAFT_EXPENSE = 23;
    public static final int DELETE_IMAGE = 24;


    void onError(String message);

    void onResponse(Object response, final int responseId);
}
