package com.expenses.epea.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.AddExpenses;
import com.expenses.epea.MenuItemsView;
import com.expenses.epea.R;
import com.expenses.epea.adapter.ExpensesDraftsAdapter;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseFragment;
import com.expenses.epea.items.ExpensesDraftListItem;
import com.expenses.epea.listener.OnDraftExpenseListener;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FrExpansesDraftList extends BaseFragment {

    View rootView;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    Unbinder unbinder;
    String[] historyType = {"Food Expense", "Food Expense", "Food Expense", "Food Expense", "Food Expense", "Food Expense"};

    ProgressDialog pd;
    String SERVER_URL;
    ArrayList<ExpensesDraftListItem> itemArrayList;
    ExpensesDraftListItem listItem;
    //FloatingActionButton approve, delete_expense;
    @BindView(R.id.approve)
    FloatingActionButton approve;
    @BindView(R.id.add_expense)
    FloatingActionButton addExpense;
    SharedPreferencesKV sharedPreferencesKV;
    ArrayList<String> ids;
    ExpensesDraftsAdapter adapter;
    @BindView(R.id.delete_expense)
    FloatingActionButton deleteExpense;
    @BindView(R.id.ll_expense)
    LinearLayout llExpense;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.expenses_draft_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        //recycleView.setAdapter(new ExpensesClaimsAdapter(Arrays.asList(historyType), getActivity()));

        sharedPreferencesKV = new SharedPreferencesKV(getActivity());
        getDraftsExpenses(getActivity());

        return rootView;
    }

    private void getDraftsExpenses(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        String userID = Common.getCommonObject().getUserId(getActivity());
        SERVER_URL = Constant.SERVER_URL + "api/employee/drafts/" + userID + "/expenses?status=1";
        //SERVER_URL = "https://www.epeaservices.co.uk/wservice/api/employee/drafts/2490/expenses?status=1";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.ALL_DRAFT_EXPENSES);
    }


    private void sendServerRequest(final Context ctx, final String url, final int responseId) {

        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(recycleView, "No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.ALL_DRAFT_EXPENSES) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            itemArrayList = new ArrayList<ExpensesDraftListItem>();
                            JSONArray jsonArray = jsonRootObject.getJSONArray("data");
                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    listItem = new ExpensesDraftListItem(obj.getString("id"), obj.getString("reference_no"),
                                            obj.getString("claim_date"), obj.getString("description"),
                                            obj.getString("notes"), obj.getString("category"),
                                            obj.getString("project"), obj.getString("net"),
                                            obj.getString("vat"), obj.getString("total"),
                                            obj.getString("currency"), obj.getString("receipts"),
                                            obj.getString("status"),
                                            obj.optString("booking_no"));

                                    itemArrayList.add(listItem);
                                }
                                adapter = new ExpensesDraftsAdapter(getActivity(), itemArrayList, new OnDraftExpenseListener() {
                                    @SuppressLint("RestrictedApi")
                                    @Override
                                    public void onselectExpense() {
                                            int countSelected = 0;
                                            for (ExpensesDraftListItem model : itemArrayList) {
                                                if (model.isSelected()) {
                                                    //return true;
                                                    countSelected++;
                                                }
                                            }
                                        if(countSelected>0){
                                            addExpense.setVisibility(View.GONE);
                                            llExpense.setVisibility(View.VISIBLE);
                                        }else{
                                            addExpense.setVisibility(View.VISIBLE);
                                            llExpense.setVisibility(View.GONE);
                                        }
                                    }
                                });
                                recycleView.setHasFixedSize(true);
                                recycleView.setAdapter(adapter);
                            } else {
                                Toast.makeText(ctx, "Expense not found", Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.approve, R.id.add_expense, R.id.delete_expense})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.approve:

                if (getSelectedIds().equals("")) {
                    Toast.makeText(getActivity(), "Select expense to approve", Toast.LENGTH_SHORT).show();
                } else {
                    sendForApproval(getActivity(), getSelectedIds());
                }
                break;
            case R.id.add_expense:
                /*for (ExpensesDraftListItem model : itemArrayList) {
                    if (model.isSelected()) {
                        model.setSelected(false);
                    }
                }
                adapter.notifyDataSetChanged();*/
                Intent i = new Intent(getActivity(), AddExpenses.class);
                startActivity(i);
                break;

            case R.id.delete_expense:
                if (getSelectedIds().equals("")) {
                    Toast.makeText(getActivity(), "Select expense to approve", Toast.LENGTH_SHORT).show();
                } else {
                    sendForDelete(getActivity(), getDeletedIds());
                }
        }
    }

    private void sendForApproval(Context ctx, String selectedIds) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        Map<String, String> params = new HashMap<String, String>();
        params.put("selected", selectedIds);
        params.put("user_id", sharedPreferencesKV.getStringValue(Common.USER_ID));

        SERVER_URL = Constant.SERVER_URL + "api/addreport";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params);
        sendRequest(ctx, SERVER_URL, params, VolleyResponseListener.APPROVE_REPORT);
    }

    private void sendForDelete(Context ctx, String selectedIds) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("draftIds", selectedIds);
        params.put("userId", sharedPreferencesKV.getStringValue(Common.USER_ID));*/
        String userId = sharedPreferencesKV.getStringValue(Common.USER_ID);

        SERVER_URL = Constant.SERVER_URL + "api/users/expenses/"+userId+"/"+selectedIds;
        //https://www.epeaservices.co.uk/wservice/api/users/expenses/:userId/:draftIds

        Log.e("request  ", "request server url:-    " + SERVER_URL );
        sendDeleteRequest(ctx, SERVER_URL, VolleyResponseListener.DELETE_DRAFTS_EXPENSE);
    }

    private void sendDeleteRequest(final Context ctx, final String url, final int responseId) {

        VolleyUtils.makeJsonGETReq(ctx, Request.Method.DELETE, url, responseId, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        pd.dismiss();
                        showSnackBar(recycleView, "No internet connection");
                    }

                    @Override
                    public void onResponse(Object response, int responseId) {
                        pd.dismiss();
                        Log.e("response  ", "response  " + response.toString());
                        if (responseId == VolleyResponseListener.DELETE_DRAFTS_EXPENSE) {
                            try {
                                JSONObject jsonRootObject = new JSONObject(response.toString());
                                if (jsonRootObject.getInt("status") == 200) {
                                    Toast.makeText(ctx, ""+jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                                    //JSONObject OBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);
                                    Intent i = new Intent(getActivity(), MenuItemsView.class);
                                    i.putExtra("type", "expense");
                                    startActivity(i);
                                    getActivity().finish();

                                } else {
                                    Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

               /* VolleyUtils.makePostReq(ctx, Request.Method.DELETE, parameters, url, responseId, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        pd.dismiss();
                        showSnackBar(recycleView, "No internet connection");
                    }

                    @Override
                    public void onResponse(Object response, int responseId) {
                        pd.dismiss();
                        Log.e("response  ", "response  " + response.toString());
                        if (responseId == VolleyResponseListener.DELETE_DRAFTS_EXPENSE) {
                            try {
                                JSONObject jsonRootObject = new JSONObject(response.toString());
                                if (jsonRootObject.getInt("status") == 200) {
                                    //JSONObject OBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);
                                    Intent i = new Intent(getActivity(), MenuItemsView.class);
                                    i.putExtra("type", "expense");
                                    startActivity(i);
                                    getActivity().finish();

                                } else {
                                    Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });*/
    }

    private void sendRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(recycleView, "No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.APPROVE_REPORT) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            //JSONObject OBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);
                            Intent i = new Intent(getActivity(), MenuItemsView.class);
                            i.putExtra("type", "report");
                            startActivity(i);
                            getActivity().finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public String getSelectedIds() {
        String selectIDS = "";
        for (ExpensesDraftListItem model : itemArrayList) {
            if (model.isSelected()) {
                //text += model.get;
                selectIDS += model.getId() + ",";
            }
        }
        if (selectIDS.contains(",")) {
            selectIDS = selectIDS.toString().substring(0, selectIDS.toString().length() - 1);
        }

        Log.d("TAG", "Output : " + selectIDS);
        return selectIDS;
    }

    public String getDeletedIds() {
        String selectIDS = "";
        for (ExpensesDraftListItem model : itemArrayList) {
            if (model.isSelected()) {
                //text += model.get;
                selectIDS += model.getId() + "|";
            }
        }
        if (selectIDS.contains("|")) {
            selectIDS = selectIDS.toString().substring(0, selectIDS.toString().length() - 1);
        }

        Log.d("TAG", "Output : " + selectIDS);
        return selectIDS;
    }
}
