package com.expenses.epea.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.AddExpenses;
import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.MenuItemsView;
import com.expenses.epea.R;
import com.expenses.epea.adapter.ApprovalAdapter;
import com.expenses.epea.adapter.ReportsAdapter;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseFragment;
import com.expenses.epea.items.ApprovalListItem;
import com.expenses.epea.items.ReportsListItem;
import com.expenses.epea.listener.OnApprovalItemListener;
import com.expenses.epea.listener.OnExpenseItemListener;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FrApproval extends BaseFragment {

    View rootView;
    String[] historyType = {"#083823472987", "#083823472987", "#083823472987", "#083823472987", "#083823472987", "#083823472987"};
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    @BindView(R.id.add_expenses)
    FloatingActionButton addExpenses;
    Unbinder unbinder;
    SharedPreferencesKV sharedPreferencesKV;
    String SERVER_URL;
    ProgressDialog pd;
    ArrayList<ApprovalListItem> itemArrayList;
    ApprovalListItem listItem;
    FragmentManager fragmentManager;
    Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dashboard_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        sharedPreferencesKV = new SharedPreferencesKV(getActivity());
        getReportExpenses(getActivity());

        //recycleView.setAdapter(new ReportsAdapter(Arrays.asList(historyType), getActivity()));
        return rootView;
    }

    private void getReportExpenses(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        String userID = Common.getCommonObject().getUserId(getActivity());
        SERVER_URL = Constant.SERVER_URL + "api/employee/approval/"+userID;
        //SERVER_URL = "https://www.epeaservices.co.uk/wservice/api/employee/reports/2490";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.APPROVAL_EXPENSES_LIST);

    }


    private void sendServerRequest(final Context ctx, final String url, final int responseId) {
        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(recycleView, "No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.APPROVAL_EXPENSES_LIST) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            itemArrayList = new ArrayList<ApprovalListItem>();
                            JSONArray jsonArray = jsonRootObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                listItem = new ApprovalListItem(obj.getString("id"),
                                        obj.getString("reference_no"), obj.getString("amount"),
                                        obj.getString("status"), obj.getString("date"),
                                        obj.getString("expenses"),obj.getString("currency"),
                                        obj.getString("emp_id"),obj.getString("emp_name"));

                                itemArrayList.add(listItem);
                            }
                            if(itemArrayList.size()<1){
                                Toast.makeText(ctx, "Expense not found", Toast.LENGTH_SHORT).show();
                            }
                            recycleView.setHasFixedSize(true);
                            recycleView.setAdapter(new ApprovalAdapter(getActivity(), itemArrayList, new OnApprovalItemListener() {
                                @Override
                                public void onclickExpense(String empID, String refID, String status) {
                                    /*Bundle args = new Bundle();
                                    args.putString("refID",refID);
                                    args.putString("empID",empID);
                                    args.putString("type","approval");
                                    ((MenuItemsView)getActivity()).setPageTitle("Expenses Claim \n#"+refID);
                                    fragmentManager = (getActivity()).getSupportFragmentManager();
                                    fragment = new FrExpansesRefrenceList();
                                    final FragmentTransaction transaction = fragmentManager.beginTransaction();
                                    fragment.setArguments(args);
                                    transaction.replace(R.id.fragment_container, fragment).commit();*/
                                    ((MenuItemsView)getActivity()).openApprovalRefrenceList(empID,refID,status);
                                    /*Intent i = new Intent(getActivity(), ExpensesDetail.class);
                                    i.putExtra("empID",empID);
                                    i.putExtra("refrenceID",refrenceID);
                                    getActivity().startActivity(i);*/
                                }

                            }));

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_expenses)
    public void onViewClicked() {
        Intent i = new Intent(getActivity(), AddExpenses.class);
        startActivity(i);
    }
}
