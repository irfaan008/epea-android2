package com.expenses.epea.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.AddExpenses;
import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.MenuItemsView;
import com.expenses.epea.R;
import com.expenses.epea.adapter.ExpensesRefrenceAdapter;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseFragment;
import com.expenses.epea.items.ExpensesRefrenceListItem;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FrExpansesRefrenceList extends BaseFragment {

    View rootView;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    Unbinder unbinder;
    String[] historyType = {"Food Expense", "Food Expense", "Food Expense", "Food Expense", "Food Expense", "Food Expense"};

    ProgressDialog pd;
    String SERVER_URL;
    ArrayList<ExpensesRefrenceListItem> itemArrayList;
    ExpensesRefrenceListItem listItem;

    SharedPreferencesKV sharedPreferencesKV;
    ArrayList<String> ids;
    ExpensesRefrenceAdapter adapter;

    @BindView(R.id.add_expenses)
    FloatingActionButton addExpenses;
    static String refrenceNO;//refID
    String userID;
    String type = "report";
    @BindView(R.id.clone_expenses)
    FloatingActionButton cloneExpenses;
    String expenseStatus;


    @BindView(R.id.approve_all)
    FloatingActionButton approveAll;
    @BindView(R.id.reject_all)
    FloatingActionButton rejectAll;
    @BindView(R.id.ll_accept_reject)
    LinearLayout llAcceptReject;

    BottomSheetDialog mBottomSheetDialog;
    View bottomSheet;

    String mRefID;
    String approvalStatus = "1";
    EditText person_name,reason;
    Button proceed;

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.expenses_refrence_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        //recycleView.setAdapter(new ExpensesClaimsAdapter(Arrays.asList(historyType), getActivity()));

        sharedPreferencesKV = new SharedPreferencesKV(getActivity());
        llAcceptReject.setVisibility(View.GONE);


        if (getArguments() != null) {
            //Toast.makeText(getActivity(), ""+getArguments().getString("refID"), Toast.LENGTH_SHORT).show();
            refrenceNO = getArguments().getString("refID");
            mRefID = getArguments().getString("refID");
            type = getArguments().getString("type");
            expenseStatus = getArguments().getString("status");
            if (getArguments().getString("empID") != null) {
                userID = getArguments().getString("empID");
            } else {
                userID = Common.getCommonObject().getUserId(getActivity());
            }

            if(type.equals("approval")){
                llAcceptReject.setVisibility(View.VISIBLE);
                addExpenses.setVisibility(View.GONE);
                cloneExpenses.setVisibility(View.GONE);
            }else{
                addExpenses.setVisibility(View.VISIBLE);
                cloneExpenses.setVisibility(View.VISIBLE);

                if (expenseStatus.equals("4")) {
                    cloneExpenses.setVisibility(View.VISIBLE);
                } else {
                    cloneExpenses.setVisibility(View.GONE);
                }
            }


        } else {
            Toast.makeText(getActivity(), "Reference id not found", Toast.LENGTH_SHORT).show();
        }

        mBottomSheetDialog = new BottomSheetDialog(getActivity());

        bottomSheet = getLayoutInflater().inflate(R.layout.popup_rejected_by, null);

        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.hide();

        person_name = (EditText) bottomSheet.findViewById(R.id.person_name);
        reason = (EditText)bottomSheet.findViewById(R.id.reason);
        proceed = (Button)bottomSheet.findViewById(R.id.proceed);
        person_name.setEnabled(false);
        person_name.setText(Common.getCommonObject().getUserName(getActivity()));

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(person_name.getText().toString().length() == 0){
                    Toast.makeText(getActivity(), "Invalid person name", Toast.LENGTH_SHORT).show();
                }else if(reason.getText().toString().length() == 0){
                    Toast.makeText(getActivity(), "Invalid Reason", Toast.LENGTH_SHORT).show();
                }else{
                    sendReject(getActivity(),person_name.getText().toString(),reason.getText().toString());
                }
            }
        });

        getRefrenceExpenses(getActivity());

        return rootView;
    }

    private void getRefrenceExpenses(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        //String userID = Common.getCommonObject().getUserId(getActivity());
        SERVER_URL = Constant.SERVER_URL + "api/employee/expenses/" + userID + "/expenses?reference_no=" + refrenceNO;
        //SERVER_URL = Constant.SERVER_URL + "api/employee/expenses/2490/expenses?reference_no=061811194110";
        //SERVER_URL = "https://www.epeaservices.co.uk/wservice/api/employee/drafts/2490/expenses?status=1";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.EXPENSE_REFRENCE_LIST);
    }


    private void sendServerRequest(final Context ctx, final String url, final int responseId) {

        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(recycleView, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.EXPENSE_REFRENCE_LIST) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            itemArrayList = new ArrayList<ExpensesRefrenceListItem>();
                            JSONArray jsonArray = jsonRootObject.getJSONArray("data");
                            approvalStatus = jsonRootObject.getString("reference_status");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                listItem = new ExpensesRefrenceListItem(obj.getString("id"), obj.getString("reference_no"),
                                        obj.getString("claim_date"), obj.getString("description"),
                                        obj.getString("notes"), obj.getString("category"),
                                        obj.getString("project"), obj.getString("net"),
                                        obj.getString("vat"), obj.getString("total"),
                                        obj.getString("currency"), obj.getString("receipts"),
                                        obj.getString("status"),
                                        obj.optString("booking_no"));

                                itemArrayList.add(listItem);
                            }
                            adapter = new ExpensesRefrenceAdapter(getActivity(), itemArrayList, userID, type);
                            recycleView.setHasFixedSize(true);
                            recycleView.setAdapter(adapter);

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                } else if (responseId == VolleyResponseListener.EXPENSE_CLONE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            //((MenuItemsView)getActivity()).openReportRefrenceList(refID);
                            Intent intent = getActivity().getIntent();
                            //intent.putExtra("","");
                            intent.putExtra("type", "expense");
                            getActivity().finish();
                            startActivity(intent);

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }else if(responseId == VolleyResponseListener.APPROVE_SINGLE_EXPENSE){
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            Toast.makeText(ctx, ""+jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            //finish();

                            Intent i = new Intent(getActivity(), MenuItemsView.class);
                            i.putExtra("type", "approval");
                            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_expenses)
    public void onViewClicked() {
        Intent i = new Intent(getActivity(), AddExpenses.class);
        startActivity(i);
    }

    /*@OnClick(R.id.clone_expenses)
    public void onClick() {
        pd = ANKProgressDialog.showLoader(getActivity(), false, "Loading..");

        SERVER_URL = Constant.SERVER_URL + "api/expenses/clones/" + userID + "/" + refrenceNO;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(getActivity(), SERVER_URL, VolleyResponseListener.EXPENSE_CLONE);
    }*/

    @OnClick({R.id.approve_all, R.id.reject_all, R.id.clone_expenses})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.approve_all:

                sendApprove(getActivity());

                break;
            case R.id.reject_all:
                mBottomSheetDialog.show();
                break;
            case R.id.clone_expenses:

                pd = ANKProgressDialog.showLoader(getActivity(), false, "Loading..");

                SERVER_URL = Constant.SERVER_URL + "api/expenses/clones/" + userID + "/" + refrenceNO;

                Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
                sendServerRequest(getActivity(), SERVER_URL, VolleyResponseListener.EXPENSE_CLONE);

                break;
        }
    }

    /*public static final int APPROVE_SINGLE_EXPENSE = 15;
    public static final int REJECT_SINGLE_EXPENSE = 16;*/
    private void sendApprove(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        String userID = Common.getCommonObject().getUserId(getActivity());
        SERVER_URL = Constant.SERVER_URL + "api/expenses/approval/"+mRefID+"/expenses?status="+approvalStatus;
        //SERVER_URL = Constant.SERVER_URL + "api/expenses/approval/061811194110/expenses?status=1";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.APPROVE_SINGLE_EXPENSE);
    }

    private void sendReject(Context ctx,String personName,String reason) {

        pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        Map<String, String> params = new HashMap<String, String>();
        params.put("reference_no", mRefID);
        params.put("rejected_by", personName);
        params.put("reason", reason);

        SERVER_URL = Constant.SERVER_URL+"api/expenses/rejected/";

        Log.e("request  ","request server url:-    "+SERVER_URL+"    "+params);
        sendPostServerRequest(ctx,SERVER_URL, params, VolleyResponseListener.REJECT_SINGLE_EXPENSE);
    }


    private void sendPostServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(llAcceptReject, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if(responseId == VolleyResponseListener.REJECT_SINGLE_EXPENSE){
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            Toast.makeText(ctx, ""+jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            //finish();

                            Intent i = new Intent(getActivity(), MenuItemsView.class);
                            i.putExtra("type", "approval");
                            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
