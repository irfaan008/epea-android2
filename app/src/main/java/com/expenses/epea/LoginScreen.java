package com.expenses.epea;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginScreen extends BaseActivity {


    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_signin)
    Button btnSignin;
    @BindView(R.id.ll_forget_pass)
    LinearLayout llForgetPass;
    @BindView(R.id.txt_forget_pass)
    TextView forgetPass;

    ProgressDialog pd;
    String SERVER_URL;
    SharedPreferencesKV sharedPreferencesKV;
    static int pass_state = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        sharedPreferencesKV = new SharedPreferencesKV(LoginScreen.this);
        etUsername.setText(sharedPreferencesKV.getStringValue(Common.USER_EMAIL_ID));
        forgetPass.setText(" "+getResources().getString(R.string.forget_pass));

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                Log.e("0000000000", "0000000000   " + event.getRawX() + "   " + etPassword.getRight() + "  " + etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width());

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etPassword.getRight() - (etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width() + 20))) {
                        // your action here
                        etPassword.requestFocus();
                        if (pass_state == 1) {

//                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            int start = etPassword.getSelectionStart();
                            int end = etPassword.getSelectionEnd();
                            etPassword.setTransformationMethod(null);
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_open, 0);
                            etPassword.setSelection(start, end);
                            pass_state = 0;
                            //                     Toast.makeText(SigninActivity.this, "showing", Toast.LENGTH_SHORT).show();
                        } else {
                            int start = etPassword.getSelectionStart();
                            int end = etPassword.getSelectionEnd();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);
                            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            etPassword.setSelection(start, end);
                            pass_state = 1;
                        }


                        return true;
                    }
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        etUsername.setText(sharedPreferencesKV.getStringValue(Common.USER_EMAIL_ID));
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick({R.id.btn_signin, R.id.ll_forget_pass})
    public void onViewClicked(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.btn_signin:
                if(etUsername.getText().toString().length() == 0){
                    showSnackBar(etUsername,"Invalid User Name");
                }else if(etPassword.getText().toString().length() == 0){
                    showSnackBar(etUsername,"Invalid Password");
                }else if(etPassword.getText().toString().length() < 6){
                    showSnackBar(etUsername,"Password should be minimum 6 digit");
                }else{
                    //showToast("Login Click");
                    sendLoginRequest(LoginScreen.this,etUsername.getText().toString(),etPassword.getText().toString());
                }
                /*i = new Intent(LoginScreen.this,OTPScreen.class);
                startActivity(i);*/
                break;
            case R.id.ll_forget_pass:
                i = new Intent(LoginScreen.this,ForgetPassword.class);
                startActivity(i);
                break;
        }
    }

    private void sendLoginRequest(Context ctx, String email, String password) {

            pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

            Map<String, String> params = new HashMap<String, String>();
            params.put("username", email);
            params.put("password", password);

            SERVER_URL = Constant.SERVER_URL+"api/auth/login";

            Log.e("request  ","request server url:-    "+SERVER_URL+"    "+params);
            sendServerRequest(ctx,SERVER_URL, params, VolleyResponseListener.LOGIN);


    }

    private void sendServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(etUsername,message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ","response  "+response.toString());
                if(responseId == VolleyResponseListener.LOGIN) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if(jsonRootObject.getInt("status") == 200){
                            //JSONObject OBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);
                            /*sharedPreferencesKV.storeLoginData(OBJ.getString("user_id"),
                                    OBJ.getString("client_id"),OBJ.getString("name"),
                                    OBJ.getString("expense"));*/
                            sharedPreferencesKV.setStringValue(Common.USER_EMAIL_ID,jsonRootObject.getJSONArray("data").getJSONObject(0).getString("email"));
                            Toast.makeText(ctx, ""+jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(LoginScreen.this,OTPScreen.class);
                            i.putExtra("email",jsonRootObject.getJSONArray("data").getJSONObject(0).getString("email"));
                            startActivity(i);
                        }else{
                            Toast.makeText(ctx, ""+Utils.statusMsg(ctx,jsonRootObject.getInt("status"),jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }
}
