package com.expenses.epea.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.R;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.items.ApprovalListItem;
import com.expenses.epea.items.ReportsListItem;
import com.expenses.epea.listener.OnApprovalItemListener;
import com.expenses.epea.listener.OnExpenseItemListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.ViewHolder> {
    //List<String> list;
    Activity activity;
    ArrayList<ApprovalListItem> itemArrayList;
    OnApprovalItemListener listener;

    /*public ReportsAdapter(List<String> list, Context ctx) {
        this.list = list;
        this.activity = (Activity) ctx;
    }*/

    public ApprovalAdapter(Activity activity, ArrayList<ApprovalListItem> itemArrayList,OnApprovalItemListener listener) {//
        this.activity = activity;
        this.itemArrayList = itemArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //holder.ref_no.setText(list.get(position));
        final ApprovalListItem listItem = (ApprovalListItem)itemArrayList.get(position);

        holder.txt_date.setText(Utils.parseDateToddMMyyyy(listItem.getDate()));
        holder.txt_amount.setText(Html.fromHtml(listItem.getCurrency())+" "+listItem.getAmount());
        /*try{

            holder.txt_amount.setText(Html.fromHtml(listItem.getCurrency())+" "+Utils.changeNoFormat(Double.valueOf(listItem.getAmount())));
        }catch (NumberFormatException e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }*/
        holder.ref_no.setText("#"+listItem.getReference_no());
        if(listItem.getStatus().equals("1")){
            holder.status.setText("In Process");
            holder.status.setTextColor(activity.getResources().getColor(R.color.on_hold_yellow));
        }else if(listItem.getStatus().equals("2")){
            holder.status.setText("Approval1");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(listItem.getStatus().equals("3")){
            holder.status.setText("Approval2");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(listItem.getStatus().equals("4")){
            holder.status.setText("Rejected");
            holder.status.setTextColor(activity.getResources().getColor(R.color.red));
        }
        holder.total_expenses.setText(listItem.getExpenses());
        holder.txt_userName.setVisibility(View.VISIBLE);
        holder.txt_userName.setText(listItem.getEmp_name());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(activity, ExpensesDetail.class);
                activity.startActivity(i);*/
                listener.onclickExpense(listItem.getEmp_id(),listItem.getReference_no(),listItem.getStatus());

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_date;
        TextView txt_amount;
        TextView ref_no;
        TextView status;
        TextView total_expenses;
        TextView txt_userName;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_amount = (TextView) itemView.findViewById(R.id.txt_amount);
            ref_no = (TextView) itemView.findViewById(R.id.ref_no);
            status = (TextView) itemView.findViewById(R.id.status);
            total_expenses = (TextView) itemView.findViewById(R.id.total_expenses);
            txt_userName = (TextView) itemView.findViewById(R.id.txt_userName);
        }
    }
}