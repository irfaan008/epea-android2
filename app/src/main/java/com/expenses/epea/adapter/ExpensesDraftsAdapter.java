package com.expenses.epea.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.expenses.epea.AddExpenses;
import com.expenses.epea.R;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.items.ExpensesDraftListItem;
import com.expenses.epea.items.ExpensesListItem;
import com.expenses.epea.listener.OnDraftExpenseListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ExpensesDraftsAdapter extends RecyclerView.Adapter<ExpensesDraftsAdapter.ViewHolder> {
    List<String> list;
    Activity activity;
    ArrayList<ExpensesDraftListItem> itemArrayList;
    AQuery aQuery;
    OnDraftExpenseListener listener;
    SharedPreferencesKV sharedPreferencesKV;
    boolean isProject;
    boolean isBookingNo;


    /*public ExpensesClaimsAdapter(List<String> list, Context ctx) {
        this.list = list;
        this.activity = (Activity) ctx;
    }*/

    public ExpensesDraftsAdapter(Activity activity, ArrayList<ExpensesDraftListItem> itemArrayList,OnDraftExpenseListener listener) {
        this.activity = activity;
        this.itemArrayList = itemArrayList;
        this.listener = listener;
        aQuery = new AQuery(activity);
        sharedPreferencesKV = new SharedPreferencesKV(activity);
        isBookingNo = sharedPreferencesKV.getBooleanValue(Common.BOOKING_ID);
        isProject = sharedPreferencesKV.getBooleanValue(Common.PROJECT);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expanse_draft_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final ExpensesDraftListItem expensesListItem = (ExpensesDraftListItem)itemArrayList.get(position);
        //String currency = expensesListItem.getCurrency();
        if(expensesListItem.getReceipts().contains(".pdf") || expensesListItem.getReceipts().contains(".PDF")){
            holder.img_expense.setImageResource(R.drawable.ic_pdf);
        }else if (expensesListItem.getReceipts().contains(".jpg") || expensesListItem.getReceipts().contains(".JPG")
                ||expensesListItem.getReceipts().contains(".JPEG") || expensesListItem.getReceipts().contains(".jpeg")
                || expensesListItem.getReceipts().contains(".png") || expensesListItem.getReceipts().contains(".PNG")){
            aQuery.id(holder.img_expense).image(expensesListItem.getReceipts());
        }else if(expensesListItem.getReceipts().toLowerCase().contains(".doc") || expensesListItem.getReceipts().toLowerCase().contains(".docs")
                || expensesListItem.getReceipts().toLowerCase().contains(".docx")){
            holder.img_expense.setImageResource(R.drawable.ic_documents);
        }else{
            holder.img_expense.setImageResource(R.drawable.image_not_found);
        }

        holder.expense_title.setText(expensesListItem.getCategory());
        /*if(expensesListItem.getStatus().equals("1")){
            holder.status.setText("Approve");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(expensesListItem.getStatus().equals("0")){
            holder.status.setText("Reject");
            holder.status.setTextColor(activity.getResources().getColor(R.color.red));
        }*/

        holder.rl_bg_view.setBackgroundDrawable(expensesListItem.isSelected() ?
                activity.getResources().getDrawable(R.drawable.rounded_view_selected) : activity.getResources().getDrawable(R.drawable.rounded_view_unselected));

        holder.expense_description.setText(expensesListItem.getDescription());
        //holder.amountVat.setText(currency+expensesListItem.getNet()+"+"+currency+expensesListItem.getVat());
        String currency = String.valueOf(Html.fromHtml(expensesListItem.getCurrency()));
        String currencyTotal = sharedPreferencesKV.getStringValue(Common.CURRENCY);
        if(isProject)
        holder.company_name.setText(expensesListItem.getProject());
        else
            holder.company_name.setText(expensesListItem.getBookingNo());
        try{
            String netAmount = Utils.changeNoFormat(Double.valueOf(expensesListItem.getNet()));
            String vatAmount = Utils.changeNoFormat(Double.valueOf(expensesListItem.getVat()));
            holder.amount_vat.setText(currency+" "+netAmount+" + "+currency+" "+vatAmount);
            holder.amount_total.setText("  = "+currencyTotal+" "+Utils.changeNoFormat(Double.valueOf(expensesListItem.getTotal())));
        }catch (NumberFormatException e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }

        holder.txt_date.setText("Date : "+ Utils.parseDateToddMMyyyy(expensesListItem.getClaim_date()));

        /*holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Intent i = new Intent(activity, ExpensesDetail.class);
                activity.startActivity(i);*//*
                //Toast.makeText(activity, ""+!expensesListItem.isSelected(), Toast.LENGTH_SHORT).show();
                expensesListItem.setSelected(!expensesListItem.isSelected());
                holder.rl_bg_view.setBackgroundDrawable(expensesListItem.isSelected() ?
                        activity.getResources().getDrawable(R.drawable.rounded_view_selected) : activity.getResources().getDrawable(R.drawable.rounded_view_unselected));
                notifyDataSetChanged();
                //itemArrayList.notifyAll();

            }
        });*/
        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                /*Intent i = new Intent(activity, ExpensesDetail.class);
                activity.startActivity(i);*/
                //Toast.makeText(activity, ""+!expensesListItem.isSelected(), Toast.LENGTH_SHORT).show();
                expensesListItem.setSelected(!expensesListItem.isSelected());
                holder.rl_bg_view.setBackgroundDrawable(expensesListItem.isSelected() ?
                        activity.getResources().getDrawable(R.drawable.rounded_view_selected) : activity.getResources().getDrawable(R.drawable.rounded_view_unselected));

                listener.onselectExpense();
                notifyDataSetChanged();
                //itemArrayList.notifyAll();
                return true;
            }
        });

        holder.expense_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity,AddExpenses.class);
                i.putExtra("type","edit");
                i.putExtra("claim_date",expensesListItem.getClaim_date());
                i.putExtra("description",expensesListItem.getDescription());
                i.putExtra("notes",expensesListItem.getNotes());
                i.putExtra("category",expensesListItem.getCategory());
                i.putExtra("project",expensesListItem.getProject());
                i.putExtra("net",expensesListItem.getNet());
                i.putExtra("vat",expensesListItem.getVat());
                i.putExtra("total",expensesListItem.getTotal());
                i.putExtra("currency",expensesListItem.getCurrency());
                i.putExtra("receipts",expensesListItem.getReceipts());
                i.putExtra("draftID",expensesListItem.getId());
                //i.putExtra("value", (Serializable) itemArrayList.get(position));
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        //return itemArrayList.size();
        return itemArrayList == null ? 0 : itemArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_expense;
        TextView expense_title;
        TextView expense_description;
        TextView company_name;
        TextView amount_vat;
        TextView amount_total;
        TextView txt_date;

        RelativeLayout rl_bg_view;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;

            img_expense = (ImageView)itemView.findViewById(R.id.img_expense);
            expense_title = (TextView) itemView.findViewById(R.id.expense_title);
            expense_description = (TextView) itemView.findViewById(R.id.expense_description);
            company_name = (TextView) itemView.findViewById(R.id.company_name);
            amount_vat = (TextView) itemView.findViewById(R.id.amount_vat);
            amount_total = (TextView) itemView.findViewById(R.id.amount_total);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);

            rl_bg_view = (RelativeLayout)itemView.findViewById(R.id.rl_bg_view);
        }
    }
}