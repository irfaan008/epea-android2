package com.expenses.epea.adapter;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.expenses.epea.R;
import com.expenses.epea.items.CatProjectItems;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class CatListAdapter extends BaseAdapter {

    ArrayList<CatProjectItems> itemsArrayList;
    Context context;
    String listType;

    public CatListAdapter(ArrayList<CatProjectItems> itemsArrayList, Context context, String listType) {
        this.itemsArrayList = itemsArrayList;
        this.context = context;
        this.listType = listType;
    }

    @Override
    public int getCount() {
        return itemsArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.cat_list_item, viewGroup, false);
        }

        // get current item to be displayed
        CatProjectItems currentItem = (CatProjectItems) getItem(i);

        // get the TextView for item name and item description
        TextView textViewItemName = (TextView)
                convertView.findViewById(R.id.txt_cat);

         if(listType.equals("Currency")){
             //sets the text for item name and item description from the current item object
             Spanned icon = Html.fromHtml(currentItem.getIcon());
             textViewItemName.setText(icon.toString()+" "+currentItem.getName());
        }else{
             //sets the text for item name and item description from the current item object
             textViewItemName.setText(currentItem.getName());
         }


        // returns the view for the current row
        return convertView;
    }
}
