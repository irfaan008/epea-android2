package com.expenses.epea.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.R;
import com.expenses.epea.items.ReportsListItem;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {
    //List<String> list;
    Activity activity;
    ArrayList<ReportsListItem> itemArrayList;

    /*public ReportsAdapter(List<String> list, Context ctx) {
        this.list = list;
        this.activity = (Activity) ctx;
    }*/

    public DashboardAdapter(Activity activity, ArrayList<ReportsListItem> itemArrayList) {
        this.activity = activity;
        this.itemArrayList = itemArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //holder.ref_no.setText(list.get(position));
        final ReportsListItem listItem = (ReportsListItem)itemArrayList.get(position);

        holder.txt_date.setText(listItem.getDate());
        holder.txt_amount.setText(Html.fromHtml(listItem.getAmount()));
        holder.ref_no.setText("#"+listItem.getReference_no());
        if(listItem.getStatus().equals("1")){
            holder.status.setText("Approve");
        }else if(listItem.getStatus().equals("2")){
            holder.status.setText("Pending");
        }
        holder.total_expenses.setText(listItem.getExpenses());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ExpensesDetail.class);
                activity.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_date;
        TextView txt_amount;
        TextView ref_no;
        TextView status;
        TextView total_expenses;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_amount = (TextView) itemView.findViewById(R.id.txt_amount);
            ref_no = (TextView) itemView.findViewById(R.id.ref_no);
            status = (TextView) itemView.findViewById(R.id.status);
            total_expenses = (TextView) itemView.findViewById(R.id.total_expenses);
        }
    }
}