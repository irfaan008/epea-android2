package com.expenses.epea.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.R;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.fragments.FrExpansesRefrenceList;
import com.expenses.epea.fragments.FrReport;
import com.expenses.epea.items.ExpensesDraftListItem;
import com.expenses.epea.items.ReportsListItem;
import com.expenses.epea.listener.OnExpenseItemListener;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.ViewHolder> {
    //List<String> list;
    Activity activity;
    ArrayList<ReportsListItem> itemArrayList;
    private FragmentManager fragmentManager;
    Fragment fragment;
    OnExpenseItemListener listener;
    SharedPreferencesKV sharedPreferencesKV;

    String SERVER_URL;
    ProgressDialog pd;
    /*public ReportsAdapter(List<String> list, Context ctx) {
        this.list = list;
        this.activity = (Activity) ctx;
    }*/

    public ReportsAdapter(Activity activity, ArrayList<ReportsListItem> itemArrayList, OnExpenseItemListener listener) {
        this.activity = activity;
        this.itemArrayList = itemArrayList;
        this.listener = listener;
        sharedPreferencesKV = new SharedPreferencesKV(activity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //holder.ref_no.setText(list.get(position));
        final ReportsListItem listItem = (ReportsListItem)itemArrayList.get(position);

        holder.txt_date.setText(Utils.parseDateToddMMyyyy(listItem.getDate()));
        /*try{

            holder.txt_amount.setText(Html.fromHtml(listItem.getCurrency())+" "+Utils.changeNoFormat(Double.valueOf(listItem.getAmount())));
        }catch (NumberFormatException e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(activity, "No Format not supported", Toast.LENGTH_SHORT).show();
        }*/
        //String currency = sharedPreferencesKV.getStringValue(Common.CURRENCY);
        holder.txt_amount.setText(Html.fromHtml(listItem.getCurrency())+" "+listItem.getAmount());

        holder.ref_no.setText("#"+listItem.getReference_no());
        holder.icon_document.setVisibility(View.GONE);
        if(listItem.getStatus().equals("1")){
            holder.status.setText("In Process");
            holder.status.setTextColor(activity.getResources().getColor(R.color.on_hold_yellow));
        }else if(listItem.getStatus().equals("2")){
            holder.status.setText("Approval1");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(listItem.getStatus().equals("3")){
            holder.status.setText("Approval2");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(listItem.getStatus().equals("4")){
            holder.status.setText("Rejected");
            holder.status.setTextColor(activity.getResources().getColor(R.color.red));
            holder.icon_document.setVisibility(View.VISIBLE);
        }
        holder.total_expenses.setText(listItem.getExpenses());

        holder.icon_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd = ANKProgressDialog.showLoader(activity, false, "Loading..");

                String userId = Common.getCommonObject().getUserId(activity);
                SERVER_URL = Constant.SERVER_URL + "api/expenses/clones/"+userId+"/"+listItem.getReference_no();

                Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
                sendServerRequest(activity, SERVER_URL, VolleyResponseListener.EXPENSE_CLONE);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(activity, ExpensesDetail.class);
                activity.startActivity(i);*/

                listener.onclickExpense(listItem.getReference_no(),listItem.getStatus());
                /*Bundle args = new Bundle();
                args.putString("refID",listItem.getReference_no());
                fragmentManager = ((FragmentActivity)activity).getSupportFragmentManager();
                fragment = new FrExpansesRefrenceList();
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                fragment.setArguments(args);
                transaction.replace(R.id.fragment_container, fragment).commit();*/

            }
        });
    }

    private void sendServerRequest(final Context ctx, final String url, final int responseId) {

        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                //showSnackBar(recycleView, message);
                Toast.makeText(ctx, "Internet Connection Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.EXPENSE_CLONE) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            Intent intent = activity.getIntent();
                            //intent.putExtra("","");
                            intent.putExtra("type", "expense");
                            activity.finish();
                            activity.startActivity(intent);
                        }else{
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_date;
        TextView txt_amount;
        TextView ref_no;
        TextView status;
        TextView total_expenses;
        ImageView icon_document;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_amount = (TextView) itemView.findViewById(R.id.txt_amount);
            ref_no = (TextView) itemView.findViewById(R.id.ref_no);
            status = (TextView) itemView.findViewById(R.id.status);
            total_expenses = (TextView) itemView.findViewById(R.id.total_expenses);
            icon_document = (ImageView)itemView.findViewById(R.id.icon_document);
        }
    }
}