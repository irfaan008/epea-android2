package com.expenses.epea.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.expenses.epea.ExpensesDetail;
import com.expenses.epea.R;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.items.ExpensesDraftListItem;
import com.expenses.epea.items.ExpensesRefrenceListItem;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ExpensesRefrenceAdapter extends RecyclerView.Adapter<ExpensesRefrenceAdapter.ViewHolder> {
    List<String> list;
    Activity activity;
    ArrayList<ExpensesRefrenceListItem> itemArrayList;
    AQuery aQuery;
    String userID;
    String expenseType;
    SharedPreferencesKV sharedPreferencesKV;
    boolean isProject;
    boolean isBookingNo;

    /*public ExpensesClaimsAdapter(List<String> list, Context ctx) {
        this.list = list;
        this.activity = (Activity) ctx;
    }*/

    public ExpensesRefrenceAdapter(Activity activity, ArrayList<ExpensesRefrenceListItem> itemArrayList,
                                   String userID,String type ) {
        this.activity = activity;
        this.itemArrayList = itemArrayList;
        this.userID = userID;
        this.expenseType = type;
        aQuery = new AQuery(activity);
        sharedPreferencesKV = new SharedPreferencesKV(activity);
        isBookingNo = sharedPreferencesKV.getBooleanValue(Common.BOOKING_ID);
        isProject = sharedPreferencesKV.getBooleanValue(Common.PROJECT);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expanse_draft_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final ExpensesRefrenceListItem expensesListItem = (ExpensesRefrenceListItem)itemArrayList.get(position);
        //String currency = expensesListItem.getCurrency();
        //aQuery.id(holder.img_expense).image(expensesListItem.getReceipts());
        if(expensesListItem.getReceipts().contains(".pdf") || expensesListItem.getReceipts().contains(".PDF")){
            holder.img_expense.setImageResource(R.drawable.ic_pdf);
        }else if (expensesListItem.getReceipts().contains(".jpg") || expensesListItem.getReceipts().contains(".JPG")
                ||expensesListItem.getReceipts().contains(".JPEG") || expensesListItem.getReceipts().contains(".jpeg")
                || expensesListItem.getReceipts().contains(".png") || expensesListItem.getReceipts().contains(".PNG")){
            aQuery.id(holder.img_expense).image(expensesListItem.getReceipts());
        }else if(expensesListItem.getReceipts().toLowerCase().contains(".doc") || expensesListItem.getReceipts().toLowerCase().contains(".docs")
                || expensesListItem.getReceipts().toLowerCase().contains(".docx")){
            holder.img_expense.setImageResource(R.drawable.ic_documents);
        }else{
            holder.img_expense.setImageResource(R.drawable.image_not_found);
        }
        holder.expense_title.setText(expensesListItem.getCategory());
        holder.expense_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        /*if(expensesListItem.getStatus().equals("1")){
            holder.status.setText("Approve");
            holder.status.setTextColor(activity.getResources().getColor(R.color.bg_green));
        }else if(expensesListItem.getStatus().equals("0")){
            holder.status.setText("Reject");
            holder.status.setTextColor(activity.getResources().getColor(R.color.red));
        }*/

        /*holder.rl_bg_view.setBackgroundDrawable(expensesListItem.isSelected() ?
                activity.getResources().getDrawable(R.drawable.rounded_view_selected) : activity.getResources().getDrawable(R.drawable.rounded_view_unselected));*/

        holder.expense_description.setText(expensesListItem.getDescription());
        //holder.amountVat.setText(currency+expensesListItem.getNet()+"+"+currency+expensesListItem.getVat());
        String currency = Html.fromHtml(expensesListItem.getCurrency()).toString();
        String currencyTotal = sharedPreferencesKV.getStringValue(Common.CURRENCY);
        if(isProject)
            holder.company_name.setText(expensesListItem.getProject());
        else
            holder.company_name.setText(expensesListItem.getBookingNo());
        holder.amount_vat.setText(currency+" "+expensesListItem.getNet()+" + "+currency+" "+expensesListItem.getVat());
        holder.amount_total.setText("  = "+currencyTotal+" "+expensesListItem.getTotal());
        holder.txt_date.setText("Date : "+ Utils.parseDateToddMMyyyy(expensesListItem.getClaim_date()));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ExpensesDetail.class);
                i.putExtra("expID",expensesListItem.getId());
                i.putExtra("userID",userID);
                i.putExtra("type",expenseType);
                activity.startActivity(i);


            }
        });
    }

    @Override
    public int getItemCount() {
        //return itemArrayList.size();
        return itemArrayList == null ? 0 : itemArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_expense;
        TextView expense_title;
        TextView expense_description;
        TextView company_name;
        TextView amount_vat;
        TextView amount_total;
        TextView txt_date;

        RelativeLayout rl_bg_view;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;

            img_expense = (ImageView)itemView.findViewById(R.id.img_expense);
            expense_title = (TextView) itemView.findViewById(R.id.expense_title);
            expense_description = (TextView) itemView.findViewById(R.id.expense_description);
            company_name = (TextView) itemView.findViewById(R.id.company_name);
            amount_vat = (TextView) itemView.findViewById(R.id.amount_vat);
            amount_total = (TextView) itemView.findViewById(R.id.amount_total);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);

            rl_bg_view = (RelativeLayout)itemView.findViewById(R.id.rl_bg_view);
        }
    }
}