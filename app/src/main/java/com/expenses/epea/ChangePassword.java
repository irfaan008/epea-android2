package com.expenses.epea;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePassword extends BaseActivity {


    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.txt)
    TextView txt;
    @BindView(R.id.current_pass)
    EditText currentPass;
    @BindView(R.id.input_current_pass)
    TextInputLayout inputCurrentPass;
    @BindView(R.id.new_pass)
    EditText newPass;
    @BindView(R.id.input_new_pass)
    TextInputLayout inputNewPass;
    @BindView(R.id.confirm_pass)
    EditText confirmPass;
    @BindView(R.id.input_confirm_pass)
    TextInputLayout inputConfirmPass;
    @BindView(R.id.submit)
    Button submit;

    ProgressDialog pd;
    String SERVER_URL;
    SharedPreferencesKV sharedPreferencesKV;
    static int pass_state_old = 1;
    static int pass_state_new = 1;
    static int pass_state_confirm = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        ButterKnife.bind(this);

        sharedPreferencesKV = new SharedPreferencesKV(ChangePassword.this);

        currentPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                Log.e("0000000000", "0000000000   " + event.getRawX() + "   " + currentPass.getRight() + "  " + currentPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width());

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (currentPass.getRight() - (currentPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width() + 20))) {
                        // your action here
                        currentPass.requestFocus();
                        if (pass_state_old == 1) {

//                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            int start = currentPass.getSelectionStart();
                            int end = currentPass.getSelectionEnd();
                            currentPass.setTransformationMethod(null);
                            currentPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_open, 0);
                            currentPass.setSelection(start, end);
                            pass_state_old = 0;
                            //                     Toast.makeText(SigninActivity.this, "showing", Toast.LENGTH_SHORT).show();
                        } else {
                            int start = currentPass.getSelectionStart();
                            int end = currentPass.getSelectionEnd();
                            currentPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);
                            currentPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            currentPass.setSelection(start, end);
                            pass_state_old = 1;
                        }


                        return true;
                    }
                }
                return false;
            }
        });


        newPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                Log.e("0000000000", "0000000000   " + event.getRawX() + "   " + newPass.getRight() + "  " + newPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width());

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (newPass.getRight() - (newPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width() + 20))) {
                        // your action here
                        newPass.requestFocus();
                        if (pass_state_new == 1) {

//                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            int start = newPass.getSelectionStart();
                            int end = newPass.getSelectionEnd();
                            newPass.setTransformationMethod(null);
                            newPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_open, 0);
                            newPass.setSelection(start, end);
                            pass_state_new = 0;
                            //                     Toast.makeText(SigninActivity.this, "showing", Toast.LENGTH_SHORT).show();
                        } else {
                            int start = newPass.getSelectionStart();
                            int end = newPass.getSelectionEnd();
                            newPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);
                            newPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            newPass.setSelection(start, end);
                            pass_state_new = 1;
                        }


                        return true;
                    }
                }
                return false;
            }
        });


        confirmPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                Log.e("0000000000", "0000000000   " + event.getRawX() + "   " + confirmPass.getRight() + "  " + confirmPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width());

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (confirmPass.getRight() - (confirmPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width() + 20))) {
                        // your action here
                        confirmPass.requestFocus();
                        if (pass_state_confirm == 1) {

//                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            int start = confirmPass.getSelectionStart();
                            int end = confirmPass.getSelectionEnd();
                            confirmPass.setTransformationMethod(null);
                            confirmPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_open, 0);
                            confirmPass.setSelection(start, end);
                            pass_state_confirm = 0;
                            //                     Toast.makeText(SigninActivity.this, "showing", Toast.LENGTH_SHORT).show();
                        } else {
                            int start = confirmPass.getSelectionStart();
                            int end = confirmPass.getSelectionEnd();
                            confirmPass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);
                            confirmPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            confirmPass.setSelection(start, end);
                            pass_state_confirm = 1;
                        }


                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick(R.id.submit)
    public void onClick() {

        if(currentPass.getText().toString().length()<6){
            Toast.makeText(this, "Current password should be more than 5 characters", Toast.LENGTH_SHORT).show();
        }else if(newPass.getText().toString().length()<6){
            Toast.makeText(this, "New password should be more than 5 characters", Toast.LENGTH_SHORT).show();
        }else if(confirmPass.getText().toString().length()<6){
            Toast.makeText(this, "Confirm new password should be more than 5 characters", Toast.LENGTH_SHORT).show();
        }else if(!confirmPass.getText().toString().equalsIgnoreCase(newPass.getText().toString())){
            Toast.makeText(this, "New password and confirm new password are not same", Toast.LENGTH_SHORT).show();
        }else{
            sendChangePasswordRequest(ChangePassword.this,currentPass.getText().toString(),newPass.getText().toString());
        }

    }

    private void sendChangePasswordRequest(Context ctx, String currentPass, String newPass) {

        pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        String userID = Common.getCommonObject().getUserId(ChangePassword.this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", userID);
        params.put("old_pass", currentPass);
        params.put("new_pass", newPass);

        SERVER_URL = Constant.SERVER_URL+"api/auth/changepassword";
        //SERVER_URL = "https://www.epeaservices.co.uk/expense-claim/wservice/api/auth/changepassword";
        //SERVER_URL = "http://www.m3s.in/eservices/wservice/api/auth/changepassword";

        Log.e("request  ","request server url:-    "+SERVER_URL+"    "+params);
        sendPostServerRequest(ctx,SERVER_URL, params, VolleyResponseListener.CHANGE_PASSWORD);


    }

    private void sendPostServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(imgLogo, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {

                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if(responseId == VolleyResponseListener.CHANGE_PASSWORD){
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            Toast.makeText(ctx, ""+jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            sharedPreferencesKV.removeUserData();
                            Intent i = new Intent(ChangePassword.this, LoginScreen.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);


                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
