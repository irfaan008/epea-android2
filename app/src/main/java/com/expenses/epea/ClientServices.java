package com.expenses.epea;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.expenses.epea.baseArchclasses.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClientServices extends BaseActivity {

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.card_payflow)
    CardView cardPayflow;
    @BindView(R.id.card_claim)
    CardView cardClaim;
    @BindView(R.id.card_invoice)
    CardView cardInvoice;
    @BindView(R.id.card_leave_calander)
    CardView cardLeaveCalander;
    @BindView(R.id.card_work_report)
    CardView cardWorkReport;
    @BindView(R.id.card_wip_report)
    CardView cardWipReport;

    FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    //public final String VERSION_CODE_KEY = "force_update_current_version";
    public final String VERSION_CODE_KEY = "android";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_services);
        ButterKnife.bind(this);

        firebaseDefaultMap = new HashMap<>();
        firebaseDefaultMap.put(VERSION_CODE_KEY, getCurrentVersionCode());
        mFirebaseRemoteConfig.setDefaults(firebaseDefaultMap);

        mFirebaseRemoteConfig.setConfigSettings(
                new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG)
                        .build());


        mFirebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                    Log.d("Firebase Update", "Fetched value: " + mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));
                    //calling function to check if new version is available or not
                    checkForUpdate(mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));
                } else {
                    Toast.makeText(ClientServices.this, "Someting went wrong please try again",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private int getCurrentVersionCode() {

        String version = "1.0";
        int verCode = 0;
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            verCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return verCode;

    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick({R.id.menu, R.id.card_payflow, R.id.card_claim, R.id.card_invoice, R.id.card_leave_calander, R.id.card_work_report, R.id.card_wip_report})
    public void onViewClicked(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.menu:
                i = new Intent(ClientServices.this,ExpencesMenuView.class);
                startActivity(i);
                break;
            case R.id.card_payflow:
                Toast.makeText(this, "You are not authorised to access this page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.card_claim:
                i = new Intent(ClientServices.this,ExpencesMenuView.class);
                startActivity(i);
                break;
            case R.id.card_invoice:
                Toast.makeText(this, "You are not authorised to access this page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.card_leave_calander:
                Toast.makeText(this, "You are not authorised to access this page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.card_work_report:
                Toast.makeText(this, "You are not authorised to access this page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.card_wip_report:
                Toast.makeText(this, "You are not authorised to access this page", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void checkForUpdate(String updateResponse) {
        //int latestAppVersion = (int) mFirebaseRemoteConfig.getDouble(VERSION_CODE_KEY);

        try {
            JSONObject jsonObject = new JSONObject(updateResponse);
            if(jsonObject.getBoolean("show_updated_popup")){
                int latestAppVersion = jsonObject.getInt("app_code");
                Log.e("updates  ","firebase updates   "+latestAppVersion+"     "+getCurrentVersionCode());
                if (latestAppVersion > getCurrentVersionCode()) {
                    new AlertDialog.Builder(this).setTitle("Please Update the App")
                            .setMessage(jsonObject.getString("update_message")).setPositiveButton(
                            "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                            /*Toast
                                    .makeText(ClientServices.this, "Take user to Google Play Store", Toast.LENGTH_SHORT)
                                    .show();*/
                                    final String appPackageName = getPackageName(); // package name of the app
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setCancelable(false).show();
                } else {
                    //Toast.makeText(this,"This app is already upto date", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
