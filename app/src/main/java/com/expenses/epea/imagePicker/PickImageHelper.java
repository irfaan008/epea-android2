package com.expenses.epea.imagePicker;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

/**
 * Created by Ankit Mishra on 11/8/2018.
 */

public class PickImageHelper {

    static String KEY_LAST_CAMERA_PHOTO = "lastPicturePath";
    static String KEY_PHOTO_URI = "CameraPictureUri";
    private static final String KEY_TYPE = "pictureTtpe";

    public static void selectImage(final Activity activity, String pdf){
        activity.startActivityForResult(getPickImageChooserIntent(activity,"pdf"), 9162);
    }


    public static Intent getPickImageChooserIntent(final Activity activity, String pdf) {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri(activity);

        /*File file = new File(getRealPathFromURI(activity,outputFileUri));
        if (file.exists())
            file.delete();*/

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            String packageName = res.activityInfo.packageName;
            Log.e("packageName ","packageName   "+packageName);
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        /*final String[] ACCEPT_MIME_TYPES = {
                "application/pdf",
                "image/*"
        };*/
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("*/*");
        //galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, ACCEPT_MIME_TYPES);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            String packageName = res.activityInfo.packageName;
            Log.e("packageName ","packageName   "+packageName);
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if(packageName.equals("com.android.documentsui")
                    || packageName.equals("com.google.android.apps.docs")
                    || packageName.equals("com.google.android.apps.photos")){

            }else{
                allIntents.add(intent);
            }


        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            //Log.e("document","document   "+intent.getComponent().getClassName());
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select File");

        chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        chooserIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        chooserIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    static Uri outputFileUri = null;
    private static Uri getCaptureImageOutputUri(Activity activity) {

        //File getImage = activity.getExternalCacheDir();

        /*if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "ImagePicked.jpeg"));
        }*/
        //File file = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis()) + ".jpg");
        //Log.e("URI"," file path uri  0000 "+file.toString());
        if(outputFileUri != null){
            return outputFileUri;
        }
        File photoFile = null;
        try {
            photoFile = createImageFile(activity);
            Log.e("URI"," file path uri  0000 "+photoFile.toString());
        } catch (IOException ex) {
            Log.e("URI"," file path uri  0000 error");
        }
        if (photoFile != null) {
            //outputFileUri = Uri.fromFile(new File(getImage.getPath(), "ImagePicked.jpeg"));
            outputFileUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", photoFile);
            Log.e("URI"," file path uri 000111  "+photoFile.toString());
        }

        Log.e("URI"," file path uri  0000222 "+outputFileUri.toString());
        return outputFileUri;
    }

    public static Uri getPickImageResultUri(Activity activity, Intent data) {
        boolean isCamera = true;
        Log.e("URI"," file path uri  action "+data.getData().toString());
        if (data != null) {
            String action = data.getAction();
            Log.e("URI"," file path uri getaction "+data.toString()+"      "+action);
            if(action != null && action.equals("inline-data")){
                isCamera = true;
            }else if(action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE)){
                isCamera = true;
            }else{
                isCamera = false;
            }
        }
        return isCamera ? getCaptureImageOutputUri(activity) : data.getData();
    }



    public static String getRealPathFromURI(Activity activity,Uri contentUri) {
        String result;
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getFilePathFromURI(Activity activity, Uri contentUri) {
        //copy file and send new file path

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + activity.getApplicationContext().getPackageName()
                + "/Files/EPEA");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
        }


        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            //File copyFile = new File(TEMP_DIR_PATH + File.separator + fileName);
            File copyFile = new File(mediaStorageDir.getAbsolutePath() + "/"+ fileName);
            copy(activity, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copyStream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static String mCurrentPhotoPath;
    private static File createImageFile(Context ctx) throws IOException {
        // Create an image file name
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + ts + "_";
        File storageDir = ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("1111111","mCurrentPhotoPath   filePathUri  "+mCurrentPhotoPath);
        return image;
    }

    public static String getCreateImagePath(){
        if(mCurrentPhotoPath != null){
            return mCurrentPhotoPath;
        }
        return null;
    }

    public enum ImageSource {
        GALLERY, DOCUMENTS, CAMERA_IMAGE, CAMERA_VIDEO
    }

    public interface Callbacks {
        void onImagePickerError(Exception e, ImageSource source, int type);

        void onImagesPicked(@NonNull List<File> imageFiles, ImageSource source, int type);

        void onCanceled(ImageSource source, int type);
    }

    private static Uri createCameraPictureFile(@NonNull Context context) throws IOException {
        File imagePath = EasyImageFiles.getCameraPicturesLocation(context);
        Uri uri = EasyImageFiles.getUriToFile(context, imagePath);
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString("CameraPictureUri", uri.toString());
        editor.putString("lastPicturePath", imagePath.toString());
        editor.apply();
        return uri;
    }

    private static void onPictureReturnedFromCamera(Activity activity, @NonNull Callbacks callbacks) {
        try {
            String lastImageUri = PreferenceManager.getDefaultSharedPreferences(activity).getString(KEY_PHOTO_URI, null);
            if (!TextUtils.isEmpty(lastImageUri)) {
                revokeWritePermission(activity, Uri.parse(lastImageUri));
            }

            File photoFile = takenCameraPicture(activity);
            List<File> files = new ArrayList<>();
            files.add(photoFile);

            if (photoFile == null) {
                Exception e = new IllegalStateException("Unable to get the picture returned from camera");
                callbacks.onImagePickerError(e, ImageSource.CAMERA_IMAGE, restoreType(activity));
            } else {
                if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("copy_taken_photos", false)) {
                    EasyImageFiles.copyFilesInSeparateThread(activity, singleFileList(photoFile));
                }

                callbacks.onImagesPicked(files, ImageSource.CAMERA_IMAGE, restoreType(activity));
            }

            PreferenceManager.getDefaultSharedPreferences(activity)
                    .edit()
                    .remove(KEY_LAST_CAMERA_PHOTO)
                    .remove(KEY_PHOTO_URI)
                    .apply();
        } catch (Exception e) {
            e.printStackTrace();
            callbacks.onImagePickerError(e, ImageSource.CAMERA_IMAGE, restoreType(activity));
        }
    }

    private static void revokeWritePermission(@NonNull Context context, Uri uri) {
        context.revokeUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

    private static void grantWritePermission(@NonNull Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    @Nullable
    private static File takenCameraPicture(Context context) {
        String lastCameraPhoto = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_LAST_CAMERA_PHOTO, null);
        if (lastCameraPhoto != null) {
            return new File(lastCameraPhoto);
        } else {
            return null;
        }
    }

    private static void storeType(@NonNull Context context, int type) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(KEY_TYPE, type).commit();
    }

    private static int restoreType(@NonNull Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(KEY_TYPE, 0);
    }

    static List<File> singleFileList(File file) {
        List<File> list = new ArrayList<>();
        list.add(file);
        return list;
    }

}
