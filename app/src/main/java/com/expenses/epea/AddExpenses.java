package com.expenses.epea;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.expenses.epea.adapter.CatListAdapter;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.CustomMultipartRequest;
import com.expenses.epea.appUtils.PickImageHelper;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.items.CatProjectItems;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class AddExpenses extends BaseActivity {

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.seleted_date)
    TextView seletedDate;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.project_code)
    TextView projectCode;
    @BindView(R.id.header_project_code)
    TextView headerProjectCode;
    @BindView(R.id.select_currency)
    TextView selectedCurrency;
    @BindView(R.id.et_bookingId)
    EditText etBookingId;
    @BindView(R.id.header_bookingId)
    TextView headerBookingId;
    /*@BindView(R.id.txt_select_msg)
    TextView imageMsg;*/
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.et_notes)
    EditText etNotes;
    @BindView(R.id.header_notes)
    TextView headerNotes;
    /*@BindView(R.id.img_upload)
    ImageView imgUpload;
    @BindView(R.id.rl_upload)
    RelativeLayout rlUpload;*/
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.ll_date)
    LinearLayout llDate;
    @BindView(R.id.ll_cat)
    LinearLayout llCat;
    @BindView(R.id.ll_project)
    LinearLayout llProject;
    @BindView(R.id.ll_currency)
    LinearLayout llCurrency;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.et_vat)
    EditText etVat;
    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;
    /*@BindView(R.id.selected_image)
    ImageView selectedImage;*/
    @BindView(R.id.et_exchangeRate)
    EditText etExchangeRate;
    @BindView(R.id.ll_upload_receipt)
    LinearLayout llUploadReceipt;
    @BindView(R.id.img_add_more)
    ImageView imgAddMore;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    static String startDate = "";
    static PickImageHelper pickImageHelper;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    private boolean bookingId;
    private boolean notes;
    private boolean project;

    int totalAmount = 0;


    PopupWindow categories, projects;
    ProgressDialog pd;
    String SERVER_URL;
    Bitmap bitmap;

    ArrayList<CatProjectItems> catArrayList;
    ArrayList<CatProjectItems> projectArrayList;
    ArrayList<CatProjectItems> currencyArrayList;
    CatProjectItems listItems;
    PopupWindow popupWindow;
    CatListAdapter adapter;
    boolean isCatSelected = false;
    SharedPreferencesKV sharedPreferencesKV;
    String netAmount = "0";
    String vatAmount = "0";
    String userID, clientID;
    static String catID, projectId = "", currencyID = "";
    File finalFile;

    String mTotalAmount = "0";
    String mAmount = "0";
    String mVat = "0";
    String type = "add";
    String mDraftID;
    AQuery aQuery;
    //int exchangeRate = 1;
    double finalExchangeRate = 0.0;
    double exchangeRates = 0.0;
    BigDecimal changeCurrency;
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    //ArrayList<File> UploadedFiles = new ArrayList<File>();
    ArrayList<UploadFilesItems> UploadedItems = new ArrayList<UploadFilesItems>();
    ArrayList<View> filesView = new ArrayList<View>();
    ArrayList<String> receiptsName = new ArrayList<String>();
    ImageView deleteReceipt;
    int currentDeletePos = 0;
    Bitmap bitMapImg = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expenses_claim);
        ButterKnife.bind(this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        aQuery = new AQuery(AddExpenses.this);

        catID = "";
        projectId = "";
        currencyID = "";

        permissions.add(CAMERA);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);

        sharedPreferencesKV = new SharedPreferencesKV(AddExpenses.this);
        userID = sharedPreferencesKV.getStringValue(Common.USER_ID);
        clientID = sharedPreferencesKV.getStringValue(Common.CLIENT_ID);
        notes = sharedPreferencesKV.getBooleanValue(Common.NOTES);
        project = sharedPreferencesKV.getBooleanValue(Common.PROJECT);
        bookingId = sharedPreferencesKV.getBooleanValue(Common.BOOKING_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        final String[] months = getResources().getStringArray(R.array.months);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(
                AddExpenses.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                seletedDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                //date.setText(year+"-"+(month+1)+"-"+dayOfMonth);

                startDate = dateFormatter.format(newDate.getTime());
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        seletedDate.setFocusable(true);

        /*submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        seletedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog.show();
            }
        });

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        projectCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        getCatList(AddExpenses.this, clientID,
                userID);


        /*if (getIntent().getExtras() != null) {
            //selectedImage.setVisibility(View.VISIBLE);
            type = getIntent().getStringExtra("type");
            //Toast.makeText(this, ""+type, Toast.LENGTH_SHORT).show();
            mDraftID = getIntent().getStringExtra("draftID");
            //Log.e("11111111","11111   "+getIntent().getSerializableExtra("value"));
            title.setText("Edit Expense");
            seletedDate.setText(getIntent().getStringExtra("claim_date"));
            category.setText(getIntent().getStringExtra("category"));
            projectCode.setText(getIntent().getStringExtra("project"));
            selectedCurrency.setText(Html.fromHtml(getIntent().getStringExtra("currency")));
            if (!getIntent().getStringExtra("receipts").equals("")) {
                //imageMsg.setTextColor(getResources().getColor(R.color.bg_green));
                if (getIntent().getStringExtra("receipts").toLowerCase().contains(".pdf")) {

                } else if (getIntent().getStringExtra("receipts").toLowerCase().contains(".doc")
                        || getIntent().getStringExtra("receipts").toLowerCase().contains(".docx")) {

                }
                checkViewImage(getIntent().getStringExtra("receipts"), "edit");

            }
            etAmount.setText(getIntent().getStringExtra("net"));
            etVat.setText(getIntent().getStringExtra("vat"));
            txtTotalAmount.setText("=" + getIntent().getStringExtra("total") + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));
            etDescription.setText(getIntent().getStringExtra("description"));
            etNotes.setText(getIntent().getStringExtra("notes"));
            mTotalAmount = add2DecimalValue(getIntent().getStringExtra("net"), getIntent().getStringExtra("vat"));
            submit.setText("Update Expense");

            netAmount = getIntent().getStringExtra("net");
            vatAmount = getIntent().getStringExtra("vat");

        } else {
            type = "add";
            //selectedImage.setVisibility(View.GONE);
        }*/


        if (getIntent().getExtras() != null) {
            type = getIntent().getStringExtra("type");
            mDraftID = getIntent().getStringExtra("draftID");
            /*if(type.equals("edit")){

                //SERVER_URL = Constant.SERVER_URL + "api/projects?client_id=" + clientID + "&user_id=" + userID;
                SERVER_URL = Constant.SERVER_URL + "api/users/expenses/"+ userID + "/" + mDraftID;

                Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
                sendServerRequest(AddExpenses.this, SERVER_URL, VolleyResponseListener.VIEW_DRAFT_EXPENSE);

            }*/
        }else {
            type = "add";
            //selectedImage.setVisibility(View.GONE);
        }
        /*String mAmount = "0";
        String mVat = "0";*/
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().startsWith(".")) {
                    etAmount.setText("");
                    Toast.makeText(AddExpenses.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    return;
                }
                mAmount = editable.toString();
                netAmount = etAmount.getText().toString().length() > 0 ? etAmount.getText().toString() : "0";
                vatAmount = etVat.getText().toString().length() > 0 ? etVat.getText().toString() : "0";
                mTotalAmount = add2DecimalValue(netAmount, vatAmount);
                txtTotalAmount.setText("= " + mTotalAmount + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));
            }
        });

        etVat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().startsWith(".")) {
                    etVat.setText("");
                    Toast.makeText(AddExpenses.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    return;
                }
                netAmount = etAmount.getText().toString().length() > 0 ? etAmount.getText().toString() : "0";
                vatAmount = etVat.getText().toString().length() > 0 ? etVat.getText().toString() : "0";
                mTotalAmount = add2DecimalValue(netAmount, vatAmount);
                txtTotalAmount.setText("= " + mTotalAmount + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));
            }
        });

        etExchangeRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Log.e("11111111","111111111111  22222 ");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(AddExpenses.this, "testttttestetst", Toast.LENGTH_SHORT).show();
                //Log.e("11111111","111111111111   ");
                if (editable.toString().startsWith(".")) {
                    etExchangeRate.setText("");
                    Toast.makeText(AddExpenses.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    return;
                }
                netAmount = etAmount.getText().toString().length() > 0 ? etAmount.getText().toString() : "0";
                vatAmount = etVat.getText().toString().length() > 0 ? etVat.getText().toString() : "0";
                exchangeRates = etExchangeRate.getText().toString().length() > 0 ? Double.valueOf(etExchangeRate.getText().toString()) : 0.0;
                mTotalAmount = add2DecimalValue(netAmount, vatAmount);
                txtTotalAmount.setText("= " + mTotalAmount + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));

                Log.e("11111111","111111111111   ");
            }
        });

        if(project){
            llProject.setVisibility(View.VISIBLE);
            projectCode.setVisibility(View.VISIBLE);
            headerProjectCode.setVisibility(View.VISIBLE);
        } else {
            llProject.setVisibility(View.GONE);
            projectCode.setVisibility(View.GONE);
            headerProjectCode.setVisibility(View.GONE);
        } if(notes){
            headerNotes.setVisibility(View.VISIBLE);
            etNotes.setVisibility(View.VISIBLE);
        } else{
            headerNotes.setVisibility(View.GONE);
            etNotes.setVisibility(View.GONE);
        } if(bookingId){
            etBookingId.setVisibility(View.VISIBLE);
            headerBookingId.setVisibility(View.VISIBLE);
        } else {
            etBookingId.setVisibility(View.GONE);
            headerBookingId.setVisibility(View.GONE);
        }
    }


    private void addView(String fileStr, String fileName, String viewType) {
        File file = new File(fileStr);
        View layout2 = getLayoutInflater().inflate(R.layout.receipt_view, null);

        ImageView uploadedImage = (ImageView) layout2.findViewById(R.id.img_doc);
        deleteReceipt = (ImageView) layout2.findViewById(R.id.delete_receipt);

        Log.e("1111111","1111111  222  "+fileStr);
        if (fileStr.toLowerCase().contains(".pdf")) {
            //imageMsg.setText("File Selected");
            uploadedImage.setImageResource(R.drawable.ic_pdf);
        } else if (fileStr.toLowerCase().contains(".jpg")
                || fileStr.toLowerCase().contains(".jpeg")
                || fileStr.toLowerCase().contains(".png")) {

            if (viewType.equals("add")) {
                //Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                //Bitmap bitmap = Common.getCommonObject().getBitmapFromAssets(getApplicationContext(),filePath);
                //Log.e("1111111","1111111  222  "+bitmap);
                if(bitMapImg != null){
                    Log.e("1111111","1111111  222 3333 ");
                    uploadedImage.setImageBitmap(bitMapImg);
                }else{
                    Log.e("1111111","1111111  222 333311  "+bitmap);
                    //String filePath = finalFile.getPath();
                    //Toast.makeText(this, "Place holder not found", Toast.LENGTH_SHORT).show();
                    Bitmap bitmap = BitmapFactory.decodeFile(finalFile.toString());
                    uploadedImage.setImageBitmap(bitmap);
                }

            } else {
                aQuery.id(uploadedImage).image(fileStr);
            }

        } else if (fileStr.toLowerCase().contains(".doc") || fileStr.toLowerCase().contains(".docs")
                || fileStr.toLowerCase().contains(".docx")) {
            uploadedImage.setImageResource(R.drawable.ic_documents);
        } else {
            uploadedImage.setImageResource(R.drawable.image_not_found);
            finalFile = null;
        }

        filesView.add(layout2);
        //Toast.makeText(this, ""+filesView.size(), Toast.LENGTH_SHORT).show();

        if (viewType.equals("add")) {
            //UploadedFiles.add(file);
            UploadFilesItems items = new UploadFilesItems(fileStr,"","add");
            UploadedItems.add(items);
        }else{
            UploadFilesItems items = new UploadFilesItems(fileStr,fileName,"edit");
            UploadedItems.add(items);
        }


        addImageVIew(deleteReceipt);

        deleteReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UploadedItems != null && UploadedItems.size()>0){
                    currentDeletePos  = (int) v.getTag();
                    //Toast.makeText(AddExpenses.this, "delete image "+currentDeletePos, Toast.LENGTH_SHORT).show();
                    UploadFilesItems items = (UploadFilesItems)UploadedItems.get(currentDeletePos);
                    if(items.getFileType().equals("add")){
                        filesView.remove(currentDeletePos);
                        UploadedItems.remove(currentDeletePos);
                    }else{
                        //if(UploadedItems.size()>1){
                            pd = ANKProgressDialog.showLoader(AddExpenses.this,false,"Loading..");
                            //SERVER_URL = Constant.SERVER_URL + "api/currency/exchange/"+ userID+"/" + to;
                            SERVER_URL = Constant.SERVER_URL + "api/expenses/images/"+ mDraftID+"/" + items.getFileName();

                            //Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
                            sendDeleteRequest(AddExpenses.this, SERVER_URL, VolleyResponseListener.DELETE_IMAGE);
                       // }

                    }

                    addImageVIew(deleteReceipt);
                }

            }
        });


        //llUploadReceipt.addView(layout2);
    }


    private void addImageVIew(ImageView deleteImg) {

        llUploadReceipt.removeAllViews();
        for(int i = 0; i<filesView.size(); i++){
            deleteImg.setTag(i);
            llUploadReceipt.addView(filesView.get(i));

        }

    }

    // onClick handler for the "X" button of each row
    public void onDeleteClicked(View v) {
        // remove the row by calling the getParent on button
        llUploadReceipt.removeView((View) v.getParent());
        //ViewGroup vg = ((ViewGroup) v.getParent());
        int index = llUploadReceipt.indexOfChild(v);
        Toast.makeText(this, ""+index, Toast.LENGTH_SHORT).show();
    }


    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }


    private void getCatList(Context ctx, String clientID, String userID) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        SERVER_URL = Constant.SERVER_URL + "api/categories?client_id=" + clientID + "&user_id=" + userID;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.GET_CATEGORIES);
    }

    private void getProjectList(Context ctx, String clientID, String userID) {

        //pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        SERVER_URL = Constant.SERVER_URL + "api/projects?client_id=" + clientID + "&user_id=" + userID;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.GET_PROJECTS);
    }

    private void getCurrencyList(Context ctx, String clientID, String userID) {

        //pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        SERVER_URL = Constant.SERVER_URL + "api/currency?client_id=" + clientID + "&user_id=" + userID;
        //SERVER_URL = "https://www.epeaservices.co.uk/expense-claim/wservice/api/currency?client_id=" + clientID + "&user_id=" + userID;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.GET_CURRENCIES);
    }

    private void getCurrencyExchange(Context ctx, String to) {

        //pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        SERVER_URL = Constant.SERVER_URL + "api/currency/exchange/"+ userID+"/" + to;
        //SERVER_URL = "https://www.epeaservices.co.uk/expense-claim/wservice/api/currency/exchange/" + userID + "/" + to;
        //SERVER_URL = "http://www.m3s.in/eservices/wservice/api/currency/exchange?user_id=" + userID+"&to=" + to;
        //SERVER_URL = "http://www.m3s.in/eservices/wservice/api/currency/exchange/"+ userID+"/" + to;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.CURRENCY_EXCHANGE);
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    /*@OnClick(R.id.submit)
    public void onViewClicked() {
    }*/

    @OnClick({R.id.ll_date, R.id.ll_cat, R.id.ll_project, R.id.ll_currency, R.id.submit, R.id.img_add_more, R.id.menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menu:
                finish();
                break;
            case R.id.ll_date:
                fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                fromDatePickerDialog.show();
                break;
            case R.id.ll_cat:
                isCatSelected = true;
                popupWindowList(AddExpenses.this, llDate, catArrayList, "Select Category", category, "Category");
                break;
            case R.id.ll_project:
                isCatSelected = false;
                popupWindowList(AddExpenses.this, llProject, projectArrayList, "Select Project", projectCode, "Project");
                break;

            case R.id.ll_currency:
                isCatSelected = false;
                popupWindowList(AddExpenses.this, llCurrency, currencyArrayList, "Select Currency", selectedCurrency, "Currency");
                break;
            case R.id.submit:
                checkValidation(AddExpenses.this);
                break;
            case R.id.img_add_more:
                //Toast.makeText(this, "rets", Toast.LENGTH_SHORT).show();
                if (hasPermission(CAMERA)) {
                    pickImageHelper.selectImage(AddExpenses.this, "pdf");
                    /*EasyImage easyImage = new EasyImage.Builder(this).setChooserTitle("Pick any Image/File")
                            .setCopyImagesToPublicGalleryFolder(false)
                            .setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
                            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                            .setFolderName("EPEA Images")
                            .allowMultiple(true).build();*/
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (permissionsToRequest.size() > 0)
                            requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
                    }
                }
                break;
        }
    }

    private void checkValidation(Context ctx) {

        /*String img = "";
        if(bitmap != null || finalFile != null){
            img = getStringImage(bitmap);
        }else {
            img = "";
            Toast.makeText(ctx, "Invalid Image", Toast.LENGTH_SHORT).show();
            return;
        }*/
        double vatAmount = 0.0;

        if (seletedDate.getText().toString().length() == 0) {
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show();
        } else if (category.getText().toString().length() == 0) {
            Toast.makeText(this, "Please Select Category", Toast.LENGTH_SHORT).show();
        } /*else if (projectCode.getText().toString().length() == 0) {
            Toast.makeText(this, "Invalid Project Code", Toast.LENGTH_SHORT).show();
        }*/ else if (etDescription.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter Description", Toast.LENGTH_SHORT).show();
        } else if (notes && etNotes.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter some note", Toast.LENGTH_SHORT).show();
        } else if(bookingId && etBookingId.getText().toString().isEmpty()){
            Toast.makeText(this, "Please enter booking number", Toast.LENGTH_SHORT).show();
        }
        else if (selectedCurrency.getText().toString().length() == 0) {
            Toast.makeText(this, "Select Currency", Toast.LENGTH_SHORT).show();
        } else if (etAmount.getText().toString().length() == 0) {
            Toast.makeText(this, "Please add net amount", Toast.LENGTH_SHORT).show();
        } else if (etVat.getText().toString().length() == 0) {
            Toast.makeText(this, "Please add vat amount", Toast.LENGTH_SHORT).show();
        } else if (!vatCheck(Utils.getPercentageValue(String.valueOf(etAmount.getText()), 20.5), Double.parseDouble(etVat.getText().toString()))) {
            Toast.makeText(ctx, "Your input VAT is more than the standard rate", Toast.LENGTH_SHORT).show();
        } else if (txtTotalAmount.getText().toString().length() == 0) {
            Toast.makeText(this, "Invalid total amount", Toast.LENGTH_SHORT).show();
        } else if (finalFile == null && type.equals("add")) {
            Toast.makeText(this, "Please Choose a file", Toast.LENGTH_SHORT).show();
        } else {

            pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

            Map<String, String> params = new HashMap<String, String>();
            params.put("claim_date", seletedDate.getText().toString());
            params.put("description", etDescription.getText().toString());
            if(notes)
            params.put("notes", etNotes.getText().toString());
            params.put("category", catID);
            if(project)
            params.put("project", projectId);
            if(bookingId)
               params.put("booking_no",etBookingId.getText().toString());
            params.put("net", etAmount.getText().toString());
            params.put("vat", etVat.getText().toString());
            params.put("total", mTotalAmount);
            params.put("currency", currencyID);
            params.put("exchange_rate", etExchangeRate.getText().toString());
            //params.put("receipts", img);


            if (type.equals("edit")) {
                /*params.put("user_id", sharedPreferencesKV.getStringValue(Common.USER_ID));
                params.put("client_id", sharedPreferencesKV.getStringValue(Common.CLIENT_ID));*/
                String mUserID = sharedPreferencesKV.getStringValue(Common.USER_ID);
                String mClientrID = sharedPreferencesKV.getStringValue(Common.CLIENT_ID);

                /*Map<String, File> paramsFile = new HashMap<String, File>();
                paramsFile.put("receipts", finalFile);*/


                SERVER_URL = Constant.SERVER_URL + "api/users/expenses/" + mClientrID + "/" + mUserID + "/" + mDraftID;
                //https://www.epeaservices.co.uk/wservice/api/users/expenses/:clientId/:userId/:draftId

                //Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params + "    " + paramsFile.toString());
                //sendAddExpensesRequest(ctx, SERVER_URL, params, paramsFile, VolleyResponseListener.EDIT_DRAFTS_EXPENSE);

                ArrayList<File> UploadedFiles = new ArrayList<File>();
                //Toast.makeText(ctx, "test  "+UploadedItems.size(), Toast.LENGTH_SHORT).show();
                for(int i=0; i<UploadedItems.size(); i++){
                    UploadFilesItems item = ((UploadFilesItems)UploadedItems.get(i));
                    if(item.getFileType().equals("add")){
                        File file  = new File(item.getReceiptStr());
                        UploadedFiles.add(file);
                    }

                }

                //Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params + "    " + UploadedFiles.toString());

                //if(UploadedFiles.size()>0){
                    Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params + "    " + UploadedFiles.toString());
                    sendAddRequest(ctx, SERVER_URL, params, UploadedFiles, VolleyResponseListener.ADD_EXPENSES);
                //}

            } else {

                params.put("user_id", sharedPreferencesKV.getStringValue(Common.USER_ID));
                params.put("client_id", sharedPreferencesKV.getStringValue(Common.CLIENT_ID));

                System.out.println(finalFile.length());

                /*Map<String, File> paramsFile = new HashMap<String, File>();
                //paramsFile.put("receipts", finalFile);
                paramsFile.put("receipts", finalFile);*/

                SERVER_URL = Constant.SERVER_URL + "api/expenses";
                //SERVER_URL = "https://www.epeaservices.co.uk/expense-claim/wservice/api/expenses";
                //Toast.makeText(ctx, "uploded items  "+UploadedItems.size(), Toast.LENGTH_SHORT).show();
                ArrayList<File> UploadedFiles = new ArrayList<File>();
                for(int i=0; i<UploadedItems.size(); i++){
                    UploadFilesItems item = ((UploadFilesItems)UploadedItems.get(i));
                    if(item.getFileType().equals("add")){
                        //Log.e("111111111","111111111   "+item.getReceiptStr());
                        File file  = new File(item.getReceiptStr());
                        UploadedFiles.add(file);
                    }

                }
                //Toast.makeText(ctx, "uploded files  "+UploadedFiles.size(), Toast.LENGTH_SHORT).show();
                if(UploadedFiles.size()>0){
                    Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params + "    " + UploadedFiles.toString());
                    sendAddRequest(ctx, SERVER_URL, params, UploadedFiles, VolleyResponseListener.ADD_EXPENSES);
                }

            }

        }

    }

    /*private void sendAddExpensesRequest(final Context ctx, final String url, Map<String, String> params, final Map<String, File> paramsFile, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, params, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(llDate,"No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                Log.e("response  ","response  "+response.toString());
                pd.dismiss();
                if(responseId == VolleyResponseListener.ADD_EXPENSES) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if(jsonRootObject.getInt("status") == 200){
                            Toast.makeText(ctx, "Expenses Added successfully", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(AddExpenses.this,MenuItemsView.class);
                            i.putExtra("type","report");
                            startActivity(i);
                            finish();

                        }else{
                            Toast.makeText(ctx, ""+ Utils.statusMsg(ctx,jsonRootObject.getInt("status"),jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }*/

    public boolean vatCheck(double d1, double d2) {//2.05,10.0
        System.out.println(Math.max(d1, d2));
        Double dR = Math.max(d1, d2);
        Log.e("test", "test   " + dR.toString() + " " + d1 + " " + d2);
        if (dR == d1) {
            return true;
        } else {
            return false;
        }
    }

    private void sendAddRequest(final Context ctx, final String server_url, Map<String, String> params, final ArrayList<File> mFilesPartData, final int responseId) {

        CustomMultipartRequest customMultipartRequest = new CustomMultipartRequest(Request.Method.POST, ctx, server_url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pd.dismiss();
                Log.e("111111111","1111111111111   111111  "+response.toString());
                if (responseId == VolleyResponseListener.ADD_EXPENSES) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            Toast.makeText(ctx, jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(AddExpenses.this, MenuItemsView.class);
                            i.putExtra("type", "expense");
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                //showSnackBar(llDate,"No internet connection");
                showSnackBar(llDate, "Server Not Responding - Add Expense");

            }
        }, mFilesPartData, params);

        customMultipartRequest.setShouldCache(false);
        customMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add the request object to the queue to be executed
        Volley.newRequestQueue(this).add(customMultipartRequest);
    }

    private void sendAddExpensesRequest(final Context ctx, final String server_url, Map<String, String> params, final Map<String, File> paramsFile, final int responseId) {

        CustomMultipartRequest customMultipartRequest = new CustomMultipartRequest(Request.Method.POST, ctx, server_url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pd.dismiss();
                if (responseId == VolleyResponseListener.ADD_EXPENSES) {

                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            Toast.makeText(ctx, jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(AddExpenses.this, MenuItemsView.class);
                            i.putExtra("type", "expense");
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                } else if (responseId == VolleyResponseListener.EDIT_DRAFTS_EXPENSE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            Toast.makeText(ctx, "" + jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(AddExpenses.this, MenuItemsView.class);
                            i.putExtra("type", "expense");
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                //showSnackBar(llDate,"No internet connection");
                showSnackBar(llDate, "Server Not Responding - Add Expense");

            }
        }, paramsFile, params);

        customMultipartRequest.setShouldCache(false);
        customMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add the request object to the queue to be executed
        Volley.newRequestQueue(this).add(customMultipartRequest);

    }

    public void popupWindowList(Context ctx, View view, final ArrayList<CatProjectItems> arrayList,
                                String mTitle, final TextView selectType, final String listType) {


        final View popupView = getLayoutInflater().inflate(R.layout.popup_listview, null);

        TextView title = (TextView) popupView.findViewById(R.id.title);
        ListView listView = (ListView) popupView.findViewById(R.id.listview);

        title.setText(mTitle);

        popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        ///popupWindow.setAnimationStyle(R.style.PopupAnimation);

        popupWindow.getContentView().setFocusableInTouchMode(true);
        popupWindow.getContentView().setFocusable(true);
        popupWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_MENU && event.getRepeatCount() == 0
                        && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });


        adapter = new CatListAdapter(arrayList, ctx, listType);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CatProjectItems items = (CatProjectItems) arrayList.get(i);
                /*if(isCatSelected){
                    category.setText(items.getName());
                }else{
                    projectCode.setText(items.getName());
                }*/

                if (listType.equals("Currency")) {
                    Spanned icon = Html.fromHtml(items.getIcon());
                    selectType.setText(icon.toString() + " " + items.getName());
                    //getCurrencyExchange(ctx,items.getId());
                    getCurrencyExchange(ctx, items.getId());
                } else {
                    selectType.setText(items.getName());
                }
                if (listType.equals("Category")) {
                    catID = items.getId();
                } else if (listType.equals("Project")) {
                    projectId = items.getId();
                } else if (listType.equals("Currency")) {
                    currencyID = items.getId();
                }


                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });

        //popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        popupWindow.showAsDropDown(view);

    }

    private void sendDeleteRequest(final Context ctx, final String url, final int responseId) {
        VolleyUtils.makeJsonGETReq(ctx, Request.Method.DELETE, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(llDate, "Server Not Responding");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                Log.e("response  ", "response  " + response.toString());
                try {
                    JSONObject jsonRootObject = new JSONObject(response.toString());
                    if (jsonRootObject.getInt("status") == 200) {

                        filesView.remove(currentDeletePos);
                        UploadedItems.remove(currentDeletePos);
                        addImageVIew(deleteReceipt);

                    } else {
                        Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    pd.dismiss();
                }
            }
        });
    }

    private void sendServerRequest(final Context ctx, final String url, final int responseId) {

        VolleyUtils.makeJsonGETReq(ctx, Request.Method.GET, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(llDate, "Server Not Responding");
            }

            @Override
            public void onResponse(Object response, int responseId) {

                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.GET_CATEGORIES) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            catArrayList = new ArrayList<CatProjectItems>();
                            JSONArray jArray = jsonRootObject.getJSONArray("data");

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject object = jArray.getJSONObject(i);
                                listItems = new CatProjectItems(object.getString("id"),
                                        object.getString("name"));
                                catArrayList.add(listItems);
                            }

                            getProjectList(AddExpenses.this, clientID, userID);
                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (responseId == VolleyResponseListener.GET_PROJECTS) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            //getProjectList(AddExpenses.this,"103","2490");
                            projectArrayList = new ArrayList<CatProjectItems>();
                            JSONArray jArray = jsonRootObject.getJSONArray("data");

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject object = jArray.getJSONObject(i);
                                listItems = new CatProjectItems(object.getString("id"),
                                        object.getString("name"));
                                projectArrayList.add(listItems);
                            }

                            getCurrencyList(AddExpenses.this, clientID, userID);
                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (responseId == VolleyResponseListener.GET_CURRENCIES) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {
                            //getProjectList(AddExpenses.this,"103","2490");
                            currencyArrayList = new ArrayList<CatProjectItems>();
                            JSONArray jArray = jsonRootObject.getJSONArray("data");

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject object = jArray.getJSONObject(i);
                                listItems = new CatProjectItems(object.getString("id"),
                                        object.getString("name"), object.getString("icon"));
                                currencyArrayList.add(listItems);
                            }

                            /*if (type.equals("edit")) {
                                catID = getCategoryID(getIntent().getStringExtra("category"));
                                projectId = getProjectID(getIntent().getStringExtra("project"));
                                String currencyIIDD = getCurrencyID(getIntent().getStringExtra("currency"));
                                currencyID = currencyIIDD == null ? "1" : currencyIIDD;
                            }*/

                            if(type.equals("edit")){

                                //SERVER_URL = Constant.SERVER_URL + "api/projects?client_id=" + clientID + "&user_id=" + userID;
                                SERVER_URL = Constant.SERVER_URL + "api/users/expenses/"+ userID + "/" + mDraftID;

                                Log.e("request  ", "request server url:-    " + SERVER_URL);
                                sendServerRequest(AddExpenses.this, SERVER_URL, VolleyResponseListener.VIEW_DRAFT_EXPENSE);

                            }

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pd != null) {
                        pd.dismiss();
                    }

                } else if (responseId == VolleyResponseListener.CURRENCY_EXCHANGE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            exchangeRates = jsonRootObject.getJSONArray("data").getJSONObject(0).getDouble("rate");
                            etExchangeRate.setText(String.valueOf(exchangeRates));

                            finalExchangeRate = Double.parseDouble(add2DecimalValue(netAmount, vatAmount));

                            finalExchangeRate = Double.parseDouble(df2.format(finalExchangeRate));
                            txtTotalAmount.setText("= " + finalExchangeRate + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));


                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pd != null) {
                        pd.dismiss();
                    }

                }else if (responseId == VolleyResponseListener.VIEW_DRAFT_EXPENSE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            JSONObject resOBJ = jsonRootObject.getJSONArray("data").getJSONObject(0);

                            seletedDate.setText(resOBJ.getString("claim_date"));
                            category.setText(getCategoryName(resOBJ.getString("category")));
                            projectCode.setText(getProjectName(resOBJ.getString("project")));
                            selectedCurrency.setText(Html.fromHtml(getCurrencyIcon(resOBJ.getString("currency"))));
                            etExchangeRate.setText(resOBJ.getString("exchange_rate"));
                            //selectedCurrency.setText(Html.fromHtml(getIntent().getStringExtra("currency")));
                            /*if (!getIntent().getStringExtra("receipts").equals("")) {
                                //imageMsg.setTextColor(getResources().getColor(R.color.bg_green));
                                if (getIntent().getStringExtra("receipts").toLowerCase().contains(".pdf")) {

                                } else if (getIntent().getStringExtra("receipts").toLowerCase().contains(".doc")
                                        || getIntent().getStringExtra("receipts").toLowerCase().contains(".docx")) {

                                }
                                checkViewImage(getIntent().getStringExtra("receipts"), "edit");

                            }*/



                            JSONArray jArray = resOBJ.getJSONArray("receipts");
                            for(int i=0; i<jArray.length(); i++){
                                receiptsName.add(jArray.getJSONObject(i).getString("filename"));
                                addView(jArray.getJSONObject(i).getString("receipt"),jArray.getJSONObject(i).getString("filename"),"edit");
                            }

                            etAmount.setText(resOBJ.getString("net"));
                            etVat.setText(resOBJ.getString("vat"));
                            txtTotalAmount.setText("=" + resOBJ.getString("total") + " " + sharedPreferencesKV.getStringValue(Common.CURRENCY));
                            etDescription.setText(resOBJ.getString("description"));
                            etNotes.setText(resOBJ.getString("notes"));
                            etBookingId.setText(resOBJ.optString("booking_no"));
                            mTotalAmount = add2DecimalValue(resOBJ.getString("net"), resOBJ.getString("vat"));
                            submit.setText("Update Expense");

                            netAmount = resOBJ.getString("net");
                            vatAmount = resOBJ.getString("vat");


                            catID = resOBJ.getString("category");
                            projectId = resOBJ.getString("project");
                            String currencyIIDD = resOBJ.getString("currency");
                            currencyID = currencyIIDD == null ? "1" : currencyIIDD;

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pd != null) {
                        pd.dismiss();
                    }

                }/*else if (responseId == VolleyResponseListener.DELETE_IMAGE) {


                }*/
            }
        });
    }

    public String getCurrencyID(String currency) {
        if (currencyArrayList != null) {
            for (int i = 0; i < currencyArrayList.size(); i++) {

                Log.e("11111111", "1111111   " + currencyArrayList.get(i).getIcon() + "  " + currency);
                if (currency.contains(currencyArrayList.get(i).getIcon())) {
                    Log.e("11111111", "1111111   " + currencyArrayList.get(i).getIcon() + "   " + currencyArrayList.get(i).getId());
                    return currencyArrayList.get(i).getId();
                }
            }
        }
        return null;
    }

    public String getCurrencyIcon(String currencyID) {
        if (currencyArrayList != null) {
            for (int i = 0; i < currencyArrayList.size(); i++) {

                Log.e("11111111", "1111111   " + currencyArrayList.get(i).getIcon() + "  " + currencyID);
                if (currencyID.equals(currencyArrayList.get(i).getId())) {
                    return currencyArrayList.get(i).getIcon();
                }
            }
        }
        return null;
    }

    public String getProjectID(String project) {
        if (projectArrayList != null) {
            for (int i = 0; i < projectArrayList.size(); i++) {

                if (project.equals(projectArrayList.get(i).getName())) {
                    return projectArrayList.get(i).getId();
                }
            }
        }
        return null;
    }

    public String getProjectName(String projectID) {
        if (projectArrayList != null) {
            for (int i = 0; i < projectArrayList.size(); i++) {

                if (projectID.equals(projectArrayList.get(i).getId())) {
                    return projectArrayList.get(i).getName();
                }
            }
        }
        return null;
    }

    public String getCategoryID(String category) {
        if (catArrayList != null) {
            for (int i = 0; i < catArrayList.size(); i++) {

                if (category.equals(catArrayList.get(i).getName())) {
                    return catArrayList.get(i).getId();
                }
            }
        }
        return null;
    }

    public String getCategoryName(String categoryID) {
        if (catArrayList != null) {
            for (int i = 0; i < catArrayList.size(); i++) {

                if (categoryID.equals(catArrayList.get(i).getId())) {
                    return catArrayList.get(i).getName();
                }
            }
        }
        return null;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        } else {
            return true;
        }
        return true;
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (hasPermission(perms)) {

                    } else {

                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());

                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    public void checkViewImage(String fileStr, String viewType) {

        /*if (fileStr.toLowerCase().contains(".pdf")) {
            imageMsg.setText("File Selected");
            selectedImage.setImageResource(R.drawable.ic_pdf);
        } else if (fileStr.toLowerCase().contains(".jpg")
                || fileStr.toLowerCase().contains(".jpeg")
                || fileStr.toLowerCase().contains(".png")) {
            imageMsg.setText("Image Selected");

            if (viewType.equals("add")) {
                Bitmap bitmap = BitmapFactory.decodeFile(finalFile.toString());
                selectedImage.setImageBitmap(bitmap);
            } else {
                aQuery.id(selectedImage).image(fileStr);
            }

            //selectedImage.setImageBitmap();
        } else if (fileStr.toLowerCase().contains(".doc") || fileStr.toLowerCase().contains(".docs")
                || fileStr.toLowerCase().contains(".docx")) {
            imageMsg.setText("File Selected");
            selectedImage.setImageResource(R.drawable.ic_documents);
        } else {
            imageMsg.setText("File not found");
            selectedImage.setImageResource(R.drawable.image_not_found);
            finalFile = null;
        }*/

        addView(fileStr,"",viewType);

    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            //Toast.makeText(this, "activity result", Toast.LENGTH_SHORT).show();
            //selectedImage.setVisibility(View.GONE);

            Uri filePathUri = pickImageHelper.getPickImageResultUri(this, data);
            Log.e("filePathUri   ", "filePathUri 22222  1111 " + filePathUri);
            if (filePathUri == null) {
                filePathUri = pickImageHelper.getImageURI();
            }
            Log.e("filePathUri   ", "filePathUri 1111 " + filePathUri);
            if (data != null) {
                if (data.getExtras() != null) {
                    Bundle extras = data.getExtras();
                    bitMapImg = (Bitmap) extras.get("data");
                } else {
                    bitMapImg = null;
                }
                //filePathUri = Uri.parse(pickImageHelper.getCreateImagePath());
            } else {
                bitMapImg = null;
            }

            try {
                if (filePathUri == null) {
                    Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                    return;
                }

                /*if (filePathUri.toString().toLowerCase().contains(".pdf") || filePathUri.toString().contains(".doc")) {
                    Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                Uri uri;
                //
                if (filePathUri.toString().contains(".pdf") || filePathUri.toString().contains(".PDF")) {
                    uri = filePathUri;
                } else if (filePathUri.toString().contains(".doc") || filePathUri.toString().contains(".docs")
                        || filePathUri.toString().contains(".docx")) {
                    uri = filePathUri;
                } else if (filePathUri.toString().toLowerCase().contains(".jpg") ||
                        filePathUri.toString().toLowerCase().contains(".jpeg") ||
                        filePathUri.toString().toLowerCase().contains(".png")) {

                    //Toast.makeText(this, ""+filePathUri.getScheme(), Toast.LENGTH_SHORT).show();
                    uri = filePathUri;
                    if (uri.toString().contains("content://com.expenses.epea.provider")) {
                        finalFile = new File(pickImageHelper.getCreateImagePath());
                        Log.e("filePathUri   ", "filePathUri 222 111 " + finalFile);
                        checkViewImage(finalFile.toString(), "add");
                        return;
                    }


                } else {

                    uri = filePathUri;
                    Log.e("filePathUri   ", "filePathUri 222 222" + uri);
                    finalFile = pickedExistingPicture(AddExpenses.this, uri);
                    Log.e("filePathUri   ", "filePathUri 3333s " + finalFile);
                    if (finalFile.toString().toLowerCase().contains(".jpg") ||
                            finalFile.toString().toLowerCase().contains(".jpeg") ||
                            finalFile.toString().toLowerCase().contains(".png")) {

                        checkViewImage(finalFile.toString(), "add");
                        return;
                    } else {
                        Toast.makeText(this, "File format not supported", Toast.LENGTH_SHORT).show();
                        return;
                    }

                }
                Log.e("filePathUri   ", "filePathUri 222 333" + uri);
                finalFile = new File(getPath(AddExpenses.this, uri));
                Log.e("filePathUri   ", "filePathUri 3333s " + finalFile);
                //uploadImage(finalFile);

                checkViewImage(finalFile.toString(), "add");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    static File pickedExistingPicture(@NonNull Context context, Uri photoUri) throws IOException {
        InputStream pictureInputStream = context.getContentResolver().openInputStream(photoUri);
        File directory = tempImageDirectory(context);
        File photoFile = new File(directory, UUID.randomUUID().toString() + "." + getMimeType(context, photoUri));
        photoFile.createNewFile();
        writeToFile(pictureInputStream, photoFile);
        return photoFile;
    }

    private static void writeToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static File tempImageDirectory(@NonNull Context context) {
        File privateTempDir = new File(context.getCacheDir(), Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + context.getPackageName()
                + "/Files/EPEA");
        if (!privateTempDir.exists()) privateTempDir.mkdirs();
        return privateTempDir;
    }

    private static String getMimeType(@NonNull Context context, @NonNull Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public String add2DecimalValue(String val1, String val2) {

        //private static DecimalFormat df2 = new DecimalFormat(".##");
        BigDecimal result = new BigDecimal(val1).add(new BigDecimal(val2));
        double totalAmount = Double.valueOf(String.valueOf(result));

        if (exchangeRates > 0) {
            double xxxx = exchangeRates * totalAmount;
            finalExchangeRate = xxxx;
        } else {
            finalExchangeRate = totalAmount;
        }

        finalExchangeRate = Double.parseDouble(df2.format(finalExchangeRate));
        //etExchangeRate.setText(String.valueOf(exchangeRates));


        Log.e("currency  ", "currency  12121212   " + finalExchangeRate + "    " + exchangeRates);

        return String.valueOf(finalExchangeRate);


        //return String.valueOf(result);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                //Toast.makeText(context, "issue yhan hai", Toast.LENGTH_SHORT).show();

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            //Toast.makeText(context, "issue yhan hai  1111", Toast.LENGTH_SHORT).show();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            //Toast.makeText(context, "issue yhan hai  2222 ", Toast.LENGTH_SHORT).show();
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            //Log.e("111111","111111111    "+uri.toString());
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            //Log.e("111111","111111111  22222222222");
            if (cursor != null)
                cursor.close();
            String contentImage = pickImageHelper.getFilePathFromURI((Activity) context, uri);
            Log.e("111111", "filePathUri contentImage " + contentImage);
            if (contentImage != null) {
                return contentImage;
            }
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    class UploadFilesItems {
        String receiptStr, fileName, fileType;

        public UploadFilesItems(String receiptStr, String fileName, String fileType) {
            this.receiptStr = receiptStr;
            this.fileName = fileName;
            this.fileType = fileType;
        }

        public String getReceiptStr() {
            return receiptStr;
        }

        public void setReceiptStr(String receiptStr) {
            this.receiptStr = receiptStr;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }
    }

}
