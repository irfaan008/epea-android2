package com.expenses.epea;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.txt_app)
    TextView txtApp;
    @BindView(R.id.txt_type)
    TextView txtType;
    @BindView(R.id.txt_edit)
    TextView txtEdit;
    @BindView(R.id.ll_edit)
    LinearLayout llEdit;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_password)
    TextView txtPassword;
    @BindView(R.id.txt_country)
    TextView txtCountry;
    @BindView(R.id.txt_currency)
    TextView txtCurrency;
    @BindView(R.id.txt_phone)
    TextView txtPhone;


    ProgressDialog pd;
    String SERVER_URL;
    @BindView(R.id.rl_change_password)
    RelativeLayout rlChangePassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.epa_profile);
        ButterKnife.bind(this);
        getProfileDetail(ProfileActivity.this);
    }

    private void getProfileDetail(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        String userID = Common.getCommonObject().getUserId(ProfileActivity.this);
        SERVER_URL = Constant.SERVER_URL + "api/employee/profile/" + userID;

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.USER_PROFILE);
    }



    private void sendServerRequest(final Context ctx, final String url, final int responseId) {
        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(menu, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.USER_PROFILE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            JSONObject profileDetail = jsonRootObject.getJSONArray("data").getJSONObject(0);
                            txtEmail.setText(profileDetail.getString("email"));
                            txtCountry.setText(profileDetail.getString("country"));
                            txtCurrency.setText(profileDetail.getString("currency"));
                            txtPhone.setText(profileDetail.getString("phone"));
                            txtType.setText(profileDetail.getString("name"));
                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick(R.id.menu)
    public void onViewClicked() {
        finish();
    }

    @OnClick(R.id.rl_change_password)
    public void onClick() {

        Intent i = new Intent(ProfileActivity.this,ChangePassword.class);
        startActivity(i);
    }
}
