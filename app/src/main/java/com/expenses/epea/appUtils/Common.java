package com.expenses.epea.appUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.Html;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ankit on 2/17/2017.
 */

public class Common {


    public static String hash_key = "";

    //https://uat.instantpay.in/static/images/operators/ACP.png

    public static String INR = "₹";
    public final static String USER_ID = "user_id";
    public final static String CLIENT_ID = "client_id";
    public final static String USER_NAME = "name";
    public final static String USER_EMAIL_ID = "emailID";
    public final static String EXPENSES = "expense";
    public final static String LOGO = "logo";
    public final static String APPROVAL = "approval";
    public final static String CURRENCY = "currency";
    public final static String PROJECT = "project";
    public final static String NOTES = "notes";
    public final static String BOOKING_ID="booking_id";

    public static Common obj;
    SharedPreferencesKV sp = null;

    public Common() {

    }

    public static Common getCommonObject() {

        if (obj == null) {

            obj = new Common();
        }
        return obj;

    }

    public String getUserId(Context ctx) {

        if (sp == null) {
            sp = new SharedPreferencesKV(ctx);
        }

        Log.d("", "sp.getStringValue(Common.USER_ID) = " + sp.getStringValue(Common.USER_ID));
        return sp.getStringValue(Common.USER_ID);

    }

    public String getUserName(Context ctx) {

        if (sp == null) {
            sp = new SharedPreferencesKV(ctx);
        }

        Log.d("", "sp.getStringValue(Common.USER_NAME) = " + sp.getStringValue(Common.USER_NAME));
        return sp.getStringValue(Common.USER_NAME);

    }

    public String getClientID(Context ctx) {

        if (sp == null) {
            sp = new SharedPreferencesKV(ctx);
        }

        Log.d("", "sp.getStringValue(Common.CLIENT_ID) = " + sp.getStringValue(Common.CLIENT_ID));
        return sp.getStringValue(Common.CLIENT_ID);

    }

    public String getExpenses(Context ctx) {

        if (sp == null) {
            sp = new SharedPreferencesKV(ctx);
        }

        Log.d("", "sp.getStringValue(Common.EXPENSES) = " + sp.getStringValue(Common.EXPENSES));
        return sp.getStringValue(Common.EXPENSES);

    }

    public String getLogo(Context ctx) {

        if (sp == null) {
            sp = new SharedPreferencesKV(ctx);
        }

        Log.d("", "sp.getStringValue(Common.LOGO) = " + sp.getStringValue(Common.LOGO));
        return sp.getStringValue(Common.LOGO);

    }

    public String getAppCurrentVersion(Context ctx) throws PackageManager.NameNotFoundException {
        PackageManager manager = ctx.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                ctx.getPackageName(), 0);
        String version = info.versionName;
        return version;
    }


    public boolean haveNetworkConnection(Context ctx) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        return haveConnectedWifi || haveConnectedMobile;
    }


    public boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void alertMesssage(final Context ctx, String title, String msg, final String url) {
        String message = String.valueOf(Html.fromHtml(msg));
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        ctx.startActivity(browserIntent);
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /*OnKYCButtonClick onKYCButtonClick;
    public void openKYCCheckBottom(Activity activity,String mTitle, String mMessage, OnKYCButtonClick buttonClick){

        onKYCButtonClick = buttonClick;
        mBottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheet = activity.getLayoutInflater().inflate(R.layout.bottom_sheet_update_kyc, null);
        TextView title = (TextView)bottomSheet.findViewById(R.id.info_head);
        TextView description = (TextView)bottomSheet.findViewById(R.id.info_msg);
        Button btn_update = (Button)bottomSheet.findViewById(R.id.btn_update);
        Button btn_later = (Button)bottomSheet.findViewById(R.id.btn_later);

        title.setText(mTitle);
        description.setText(mMessage);

        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onKYCButtonClick.onUpdate("");
            }
        });

        btn_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onKYCButtonClick.onLater("");
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }
    public void dismissKYC(){
        if(mBottomSheetDialog != null){
            mBottomSheetDialog.dismiss();
        }
    }*/

    public Bitmap getBitmapFromAssets(Context context, String fileName) {
        AssetManager asset = context.getAssets();
        InputStream is;
        try {
            is = asset.open(fileName);
        } catch (IOException e) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, options);
        try {
            is.reset();
        } catch (IOException e) {
            return null;
        }

        //options.inSampleSize = calculateInSampleSize(options, width, height);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(is, null, options);
    }


}
