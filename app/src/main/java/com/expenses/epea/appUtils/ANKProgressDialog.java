package com.expenses.epea.appUtils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

public class ANKProgressDialog {

    public ANKProgressDialog() {
    }

    public static ProgressDialog showLoader(Context context, boolean cancelFlag,String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.requestWindowFeature(1);
        progressDialog.setCancelable(cancelFlag);
        progressDialog.setMessage(message);
        progressDialog.show();
        return progressDialog;
    }

    public static void dismissLoader(Dialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }
}
