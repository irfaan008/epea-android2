package com.expenses.epea.appUtils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ankit Mishra on 14/11/2018.
 */

public class CustomMultipartRequest extends Request<JSONObject> {

    private final Response.Listener<JSONObject> mListener;
    private Map<String, File> mFilePartData;
    private ArrayList<File> mFilesPartData;
    private final Map<String, String> mStringPart;
    //private final Map<String, String> mHeaderPart;

    private MultipartEntityBuilder mEntityBuilder = MultipartEntityBuilder.create();
    private HttpEntity mHttpEntity;
    private Context mContext;
    MultipartEntity entity = new MultipartEntity(
            HttpMultipartMode.BROWSER_COMPATIBLE);

    public CustomMultipartRequest(int method, Context mContext, String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener,
                                  Map<String, File> mFilePartData, Map<String, String> mStringPart) {
        super(method, url, errorListener);
        mListener = listener;
        this.mFilePartData = mFilePartData;
        this.mStringPart = mStringPart;
        this.mContext = mContext;
        mEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartFileEntity();
        buildMultipartTextEntity();
        mHttpEntity = mEntityBuilder.build();
    }

    public CustomMultipartRequest(int method, Context mContext, String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener,
                                  ArrayList<File> mFilesPartData, Map<String, String> mStringPart) {
        super(method, url, errorListener);
        mListener = listener;
        this.mFilesPartData = mFilesPartData;
        this.mStringPart = mStringPart;
        this.mContext = mContext;
        mEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        buildMultipartFilesEntity(mFilesPartData);
        buildMultipartTextEntity();
        mHttpEntity = mEntityBuilder.build();
    }



    public static String getMimeType(Context context, String url) {
        Uri uri = Uri.fromFile(new File(url));
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;


    }

    private void buildMultipartFileEntity() {
        for (Map.Entry<String, File> entry : mFilePartData.entrySet()) {
            try {

                String key = entry.getKey();
                File file = entry.getValue();
                Log.e("11111111111","1111111111  "+file.toString());
                String mimeType = getMimeType(mContext, file.toString());
                Log.e("11111111111","1111111111  "+mimeType.toString());
                //mEntityBuilder.addBinaryBody(key, file, ContentType.create(mimeType), file.getName());
                mEntityBuilder.addBinaryBody(key, file, ContentType.create(mimeType), file.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildMultipartFilesEntity(ArrayList<File> files) {
        for (int i=0; i < files.size();i++) {
            try {

                //String key = entry.getKey();
                //File file = entry.getValue();
                Log.e("11111111111","1111111111  "+files.get(i).toString());
                String mimeType = getMimeType(mContext, files.get(i).toString());
                Log.e("11111111111","1111111111  "+mimeType.toString());
                //mEntityBuilder.addBinaryBody(key, file, ContentType.create(mimeType), file.getName());
                FileBody fileBody = new FileBody(files.get(i));
                //mEntityBuilder.addBinaryBody(key, files.get(i), ContentType.create(mimeType), files.get(i).toString());
                mEntityBuilder.addPart("receipts[]",fileBody);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void buildMultipartTextEntity() {
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key != null && value != null)
                mEntityBuilder.addTextBody(key, value);
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> header = new HashMap<String, String>();
        header.put("Accept", "application/json");
        //header.put("Content-Type", "application/x-www-form-urlencoded");
        return header;
    }


    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        Log.e("111111111","11111111  "+response.toString());
        mListener.onResponse(response);
    }
}
