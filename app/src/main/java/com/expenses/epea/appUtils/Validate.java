package com.expenses.epea.appUtils;


import java.util.regex.Pattern;

/**
 * Created by mayank on 7/12/16.
 */
public class Validate {

    private String type;
    private String text;

    public Validate(){

    }


    public boolean validate(String pattern, String string){
        this.type = pattern;
        this.text = string;

        if(type.equalsIgnoreCase("mobile")){
            if(text.matches("^[456789]\\d{9}$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("email")){
            if(text.trim().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("name")){
            if(text.trim().matches("^[\\p{L} .'-]+$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("pincode")){
            if(text.matches("^[1-9][0-9]{5}$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("otp")){
            if(text.trim().matches("^[0-9]{1,6}$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("password")){
            if(text.trim().length() == 0){
                return false;
            }
            if(text.length() > 5){
                return true;
            }
        }else if(type.equalsIgnoreCase("numeric")){
            if(text.matches("[-+]?\\d*\\.?\\d+")){
                return true;
            }
        }else if(type.equalsIgnoreCase("alnum")){
            if(text.matches("^[a-zA-Z0-9 " +
                    "" +
                    "]*$")){
                return true;
            }
        }else if(type.equalsIgnoreCase("panCard")){
            Pattern patt = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
            if(text.matches("[A-Z]{5}[0-9]{4}[A-Z]{1}") && text.trim().length() == 10){
                return true;
            }
        }else if(type.equalsIgnoreCase("aadharCard")){//[A-Z]{5}[0-9]{4}[A-Z]{1}
            if(text.matches("^[a-zA-Z0-9 " +
                    "" +
                    "]*$") && text.trim().length() == 12){
                return true;
            }
        }else{
            return false;
        }

        return false;

    }

}
