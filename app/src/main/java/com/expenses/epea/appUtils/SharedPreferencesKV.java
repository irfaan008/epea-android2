package com.expenses.epea.appUtils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesKV {
	
	public static SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private static String FILENAME = "InstantPayCards";
	Context context;
	
	public SharedPreferencesKV(Context ctx){

		settings = ctx.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		editor = settings.edit();
		context = ctx;
		
	}
	
	public void setStringValue(String key, String value)
	{
		editor.putString(key, value);
		editor.commit();
	}
	
	public void setIntValue(String key, int value)
	{
		editor.putInt(key, value);
		editor.commit();
	}

	public void setBooleanValue(String key, boolean value)
	{
		editor.putBoolean(key, value);
		editor.commit();
	}

	public String getStringValue(String key)
	{
		return settings.getString(key, "");

	}
	
	public static int getIntValue(String key)
	{
		return settings.getInt(key, 0);

	}
	public static boolean getBooleanValue(String key)
	{
		return settings.getBoolean(key, false);

	}


	public void storeLoginData(String userID , String clientID, String name, String expense, String logo,
			String approval, String currency, boolean project, boolean notes, boolean bookingNo){
		this.setStringValue(Common.USER_ID, userID);
		this.setStringValue(Common.CLIENT_ID, clientID);
		this.setStringValue(Common.USER_NAME, name);
		this.setStringValue(Common.EXPENSES, expense);
		this.setStringValue(Common.LOGO, logo);
		this.setStringValue(Common.APPROVAL, approval);
		this.setStringValue(Common.CURRENCY, currency);
		this.setBooleanValue(Common.PROJECT, project);
		this.setBooleanValue(Common.NOTES, notes);
		this.setBooleanValue(Common.BOOKING_ID, bookingNo);
	}

	public void removeUserData(){

		setStringValue(Common.USER_ID, null);
		setStringValue(Common.CLIENT_ID, null);
		setStringValue(Common.USER_NAME, null);
		setStringValue(Common.EXPENSES, null);
		setStringValue(Common.LOGO, null);
		setStringValue(Common.APPROVAL, null);
		this.setStringValue(Common.CURRENCY, null);
	}
}
