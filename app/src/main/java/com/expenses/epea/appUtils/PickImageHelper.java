package com.expenses.epea.appUtils;

import android.app.Activity;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.core.content.FileProvider;

/**
 * Created by Ankit Mishra on 11/8/2018.
 */

public class PickImageHelper {

    static String mCurrentPhotoPath;
    static Uri outputFileUri = null;

    public static void selectImage(final Activity activity, String pdf){
        mCurrentPhotoPath = null;
        outputFileUri = null;
        Intent resultIntent = getPickImageChooserIntent(activity,"pdf");
        Log.e("intent ","result intent   "+resultIntent.getData());
        activity.startActivityForResult(resultIntent, 9162);
    }


    public static Intent getPickImageChooserIntent(final Activity activity, String pdf) {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri(activity);

        /*File file = new File(getRealPathFromURI(activity,outputFileUri));
        if (file.exists())
            file.delete();*/

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        //captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            String packageName = res.activityInfo.packageName;
            Log.e("packageName ","packageName   "+packageName);
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            Log.e("packageName ","outputFileUri   "+outputFileUri);

            /*if(!packageName.toString().contains("camera2")){
                Log.e("packageName ","packageName  test1  "+packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }*/

            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            //intent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
            //Log.e("packageName ","outputFileUri111111   "+MyFileContentProvider.CONTENT_URI);
            allIntents.add(intent);
        }

        Log.e("packageName ","outputFileUri  intent111   "+allIntents.toString());

        // collect all gallery intents
        /*final String[] ACCEPT_MIME_TYPES = {
                "application/pdf",
                "image/*"
        };*/
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("*/*");
        //galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, ACCEPT_MIME_TYPES);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            String packageName = res.activityInfo.packageName;
            Log.e("packageName ","packageName   "+packageName);
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if(packageName.equals("com.android.documentsui")
                    || packageName.equals("com.google.android.apps.docs")
                    || packageName.equals("com.google.android.apps.photos")){

            }else{
                allIntents.add(intent);
            }


        }

        // the main intent is the last in the list (fucking android) so pickup the useless one

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            Log.e("document","document   "+intent.getComponent().getClassName());
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }

        allIntents.remove(mainIntent);

        Log.e("packageName ","outputFileUri  intent 22222  "+allIntents.toString());
        Log.e("packageName ","outputFileUri  intent 22222  "+mainIntent.getData());

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select File");

        chooserIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        chooserIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

        Log.e("packageName ","outputFileUri  intent 3333  "+chooserIntent.getData());
        Log.e("packageName ","outputFileUri  intent 3333  "+chooserIntent.getExtras());

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */

    private static Uri getCaptureImageOutputUri(Activity activity) {
        /*Uri outputFileUri = null;
        File getImage = activity.getExternalCacheDir();

        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "ImagePicked.jpeg"));
        }
        File file = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis()) + ".jpg");
        Log.e("URI"," file path uri  0000 "+file.toString());*/


        if(outputFileUri != null){
            return outputFileUri;
        }
        File photoFile = null;
        try {
            photoFile = createImageFile(activity);
            Log.e("URI"," file path uri  0000 "+photoFile.toString());
        } catch (IOException ex) {
            Log.e("URI"," file path uri  0000 error");
        }
        if (photoFile != null) {
            //outputFileUri = Uri.fromFile(new File(getImage.getPath(), "ImagePicked.jpeg"));
            outputFileUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", photoFile);
            Log.e("URI"," file path uri 000111  "+photoFile.toString());
        }

        Log.e("URI"," file path uri  0000222 "+outputFileUri.toString());
        return outputFileUri;
    }

    public static Uri getPickImageResultUri(Activity activity, Intent data) {
        boolean isCamera = true;
        //Log.e("URI"," file path uri  action "+data.toString());
        if (data != null) {
            //Bundle extras = data.getExtras();
            //Log.e("URI"," file path uri getaction "+extras.toString());

            //Bitmap photo = (Bitmap) data.getExtras().get("data");
            //Log.e("URI"," file path uri getaction "+photo.toString());
            String action = data.getAction();
            Log.e("URI"," file path uri getaction "+data.toString()+"      "+action);
            if(action != null && action.equals("inline-data")){
                isCamera = true;
            }else if(action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE)){
                isCamera = true;
            }else{
                isCamera = false;
            }
        }
        return isCamera ? getCaptureImageOutputUri(activity) : getGalleryImageUri(data);
    }

    public static Uri getGalleryImageUri(Intent data){

        try {
            ClipData clipData = data.getClipData();
            List<File> files = new ArrayList<>();
            if (clipData == null) {
                Uri uri = data.getData();
                return uri;
                /*File file = EasyImageFiles.pickedExistingPicture(activity, uri);
                files.add(file);*/
            } else {
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    Uri uri = clipData.getItemAt(i).getUri();
                    return uri;
                    /*File file = EasyImageFiles.pickedExistingPicture(activity, uri);
                    files.add(file);*/
                }
            }

            /*if (configuration(activity).shouldCopyPickedImagesToPublicGalleryAppFolder()) {
                EasyImageFiles.copyFilesInSeparateThread(activity, files);
            }

            callbacks.onImagesPicked(files, ImageSource.GALLERY, restoreType(activity));*/
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error"," error found in gallery  "+e.toString());
        }
        return null;
    }



    public static String getRealPathFromURI(Activity activity,Uri contentUri) {
        String result;
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getFilePathFromURI(Activity activity, Uri contentUri) {
        //copy file and send new file path

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + activity.getApplicationContext().getPackageName()
                + "/Files/EPEA");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
        }


        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            //File copyFile = new File(TEMP_DIR_PATH + File.separator + fileName);
            File copyFile = new File(mediaStorageDir.getAbsolutePath() + "/"+ fileName);
            copy(activity, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copyStream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File createImageFile(Context ctx) throws IOException {
        // Create an image file name
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + ts + "_";
        File storageDir = ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("1111111","mCurrentPhotoPath   filePathUri  "+mCurrentPhotoPath);
        return image;
    }

    public static String getCreateImagePath(){
        if(mCurrentPhotoPath != null){
            return mCurrentPhotoPath;
        }
        return null;
    }

    public static Uri getImageURI(){
        if(outputFileUri != null){
            return outputFileUri;
        }
        return null;
    }

}
