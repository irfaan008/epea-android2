package com.expenses.epea.baseArchclasses;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.expenses.epea.appUtils.ANKProgressDialog;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements BaseMvpView,BaseFragment.Callback {



    @Override
    public void hideKeyboard() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void showAlert(int resId) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showErrorAlert() {

    }

    @Override
    public void showNoInternetAlert() {

    }

    @Override
    public void showKeyBoard() {

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int resId) {

    }

    @Override
    public void showToast(int resId, int fieldName) {

    }

    @Override
    public void showToast(int resId, String message) {

    }

    @Override
    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}
