package com.expenses.epea.baseArchclasses;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import androidx.annotation.StringRes;

public interface BaseMvpView
{
    void hideKeyboard();

    void hideLoading();

    boolean isNetworkConnected();

    void showAlert(@StringRes int resId);

    void showAlert(String message);

    void showErrorAlert();

    /*void showLoading();

    void showLoading(Context ctx, boolean flag, String message);

    void dismissLoading(Dialog progressDialog);*/

    void showNoInternetAlert();

    void showKeyBoard();

    void showToast(String message);

    void showToast(@StringRes int resId);

    void showToast(@StringRes int resId, @StringRes int fieldName);

    void showToast(@StringRes int resId, String message);

    void showSnackBar(View view,  String message);
    //void showSnackBar(@StringRes int resId, String message);
}
