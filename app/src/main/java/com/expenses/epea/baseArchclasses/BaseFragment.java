package com.expenses.epea.baseArchclasses;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment implements BaseMvpView{

    private BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void showAlert(int resId) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void showErrorAlert() {

    }

    @Override
    public void showNoInternetAlert() {

    }

    @Override
    public void showKeyBoard() {

    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void showToast(int resId) {

    }

    @Override
    public void showToast(int resId, int fieldName) {

    }

    @Override
    public void showToast(int resId, String message) {

    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }
}
