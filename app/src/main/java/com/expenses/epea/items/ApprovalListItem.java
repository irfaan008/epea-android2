package com.expenses.epea.items;

public class ApprovalListItem {

    String id,reference_no,amount,status,date,expenses,currency,emp_id,emp_name;

    public ApprovalListItem(String id, String reference_no, String amount,
                            String status, String date, String expenses, String currency,
                            String emp_id,String emp_name) {
        this.id = id;
        this.reference_no = reference_no;
        this.amount = amount;
        this.status = status;
        this.date = date;
        this.expenses = expenses;
        this.currency = currency;
        this.emp_id = emp_id;
        this.emp_name = emp_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }
}
