package com.expenses.epea.items;

public class DashboardListItem {

    String id,reference_no,amount,status,date,expenses;

    public DashboardListItem(String id, String reference_no, String amount,
                             String status, String date, String expenses) {
        this.id = id;
        this.reference_no = reference_no;
        this.amount = amount;
        this.status = status;
        this.date = date;
        this.expenses = expenses;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }
}
