package com.expenses.epea.items;

public class CatProjectItems {

    String id,name;
    String icon;

    public CatProjectItems(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public CatProjectItems(String id, String name, String mIcon) {
        this.id = id;
        this.name = name;
        this.icon = mIcon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
