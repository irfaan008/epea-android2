package com.expenses.epea;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.google.android.material.snackbar.Snackbar;


public class SplashScreenActivity extends Activity
{

    private static int SPLASH_TIME_OUT = 1500;
    SharedPreferencesKV sharedPreferences;
    RelativeLayout rl_view;
    ProgressBar progressBar2;
    String customerID;
    String username,userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        rl_view = (RelativeLayout)findViewById(R.id.rl_view);


        sharedPreferences = new SharedPreferencesKV(SplashScreenActivity.this);

        proceedView();
    }

    public void proceedView(){

        //StartAnimations();
        if (isNetworkAvailable(SplashScreenActivity.this)){
            StartAnimations();
        }else{
            showSnakeBar("No internet connection!");
        }
    }

    private void StartAnimations() {


        username = Common.getCommonObject().getUserName(SplashScreenActivity.this);
        userID = Common.getCommonObject().getUserId(SplashScreenActivity.this);

        Log.e("username","username  "+username);
        Log.e("token","token  "+userID);

        if (username.isEmpty() || userID.isEmpty()) {
            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {

                    startActivity(new Intent(SplashScreenActivity.this, LoginScreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();

                }
            }, SPLASH_TIME_OUT);

        } else {

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {

                    startActivity(new Intent(SplashScreenActivity.this, ClientServices.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();

                }
            },SPLASH_TIME_OUT);

        }

    }
    private boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showSnakeBar(String message){
        //progressBar2.setVisibility(View.GONE);
        Snackbar snackbar = Snackbar
                .make(rl_view, message, Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isNetworkAvailable(SplashScreenActivity.this)){
                            StartAnimations();
                            //progressBar2.setVisibility(View.VISIBLE);
                        }else{
                            showSnakeBar("No internet connection!");

                        }
                    }
                });

// Changing message text color
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Application.getInstance().setConnectivityListener(this);

        try {
            PackageInfo pInfo = SplashScreenActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
    }
}