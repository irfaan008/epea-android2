package com.expenses.epea;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPassword extends BaseActivity {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.submit)
    Button submit;

    String SERVER_URL;
    ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        ButterKnife.bind(this);
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick(R.id.submit)
    public void onViewClicked() {

        if(email.getText().toString().length() == 0){
            showSnackBar(email,"Invalid email id");
        }else{
            //showToast("Submit email Click");
            sendLoginRequest(ForgetPassword.this, email.getText().toString());
        }

    }

    private void sendLoginRequest(Context ctx, String email) {

        pd = ANKProgressDialog.showLoader(ctx,false,"Loading..");

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);

        SERVER_URL = Constant.SERVER_URL+"api/auth/reset";

        Log.e("request  ","request server url:-    "+SERVER_URL+"    "+params);
        sendServerRequest(ctx,SERVER_URL, params, VolleyResponseListener.FORGET_PASSWORD);


    }

    private void sendServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(email,"No internet connection");
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ","response  "+response.toString());
                if(responseId == VolleyResponseListener.FORGET_PASSWORD) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if(jsonRootObject.getInt("status") == 200){
                            Toast.makeText(ctx, "Link has Send to mail", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(ctx, ""+ Utils.statusMsg(ctx,jsonRootObject.getInt("status"),jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }
}
