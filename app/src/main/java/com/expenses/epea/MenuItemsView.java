package com.expenses.epea;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenses.epea.appUtils.BackHandledFragment;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.fragments.FrApproval;
import com.expenses.epea.fragments.FrDashboardView;
import com.expenses.epea.fragments.FrExpansesDraftList;
import com.expenses.epea.fragments.FrExpansesRefrenceList;
import com.expenses.epea.fragments.FrReport;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuItemsView extends FragmentActivity implements BackHandledFragment.BackHandlerInterface{

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.title)
    TextView title;

    private FragmentManager fragmentManager;
    private BackHandledFragment selectedFragment;
    Fragment fragment;

    String viewType;
    static String currentFrag = "Dashboard";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.administration_main_view);
        ButterKnife.bind(this);

        Bundle args = new Bundle();

        fragmentManager = getSupportFragmentManager();
        viewType = getIntent().getStringExtra("type");

        if (viewType.equals("dashboard")) {
            title.setText("Dashboard");
            fragment = new FrDashboardView();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment.setArguments(args);
            transaction.replace(R.id.fragment_container, fragment).commit();
            currentFrag = "Dashboard";
            /*fragment = new FrReport();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment.setArguments(args);
            transaction.replace(R.id.fragment_container, fragment).commit();*/
        }else if(viewType.equals("expense")){
            title.setText("Expenses Drafts");
            fragment = new FrExpansesDraftList();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment.setArguments(args);
            transaction.replace(R.id.fragment_container, fragment).commit();
            currentFrag = "Expenses Drafts";
        }else if(viewType.equals("report")){
            title.setText("Reports");
            fragment = new FrReport();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment.setArguments(args);
            transaction.replace(R.id.fragment_container, fragment).commit();
            currentFrag = "Reports";
        }else if(viewType.equals("approval")){
            title.setText("Approval");
            fragment = new FrApproval();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment.setArguments(args);
            transaction.replace(R.id.fragment_container, fragment).commit();
            currentFrag = "Approval";
        }

    }


    public void openReportRefrenceList(String refID, String status){
        Bundle args = new Bundle();
        args.putString("refID",refID);
        args.putString("type","report");
        args.putString("status",status);
        //((MenuItemsView)getActivity()).setPageTitle("Expenses Claim \n#"+refID);
        //fragmentManager = (getActivity()).getSupportFragmentManager();
        title.setText("Expenses Claim \n#"+refID);
        fragment = new FrExpansesRefrenceList();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragment.setArguments(args);
        transaction.replace(R.id.fragment_container, fragment).addToBackStack("reportReferences").commit();
    }

    public void openApprovalRefrenceList(String empID,String refID, String status){
        Bundle args = new Bundle();
        args.putString("refID",refID);
        args.putString("empID",empID);
        args.putString("type","approval");
        args.putString("status",status);
//        ((MenuItemsView)getActivity()).setPageTitle("Expenses Claim \n#"+refID);
//        fragmentManager = (getActivity()).getSupportFragmentManager();
        title.setText("Expenses Claim \n#"+refID);
        fragment = new FrExpansesRefrenceList();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragment.setArguments(args);
        transaction.replace(R.id.fragment_container, fragment).addToBackStack("approvalReferences").commit();
    }


    public void setPageTitle(String mTitle){
        title.setText(mTitle);
    }

    @OnClick({R.id.menu})
    public void onViewClicked(View view) {
        //Intent i;
        switch (view.getId()) {
            case R.id.menu:
                //finish();
                Intent i = new Intent(MenuItemsView.this,ExpencesMenuView.class);
                startActivity(i);
                break;
        }
    }

    @Override
    public void setSelectedFragment(BackHandledFragment backHandledFragment) {
        this.selectedFragment = backHandledFragment;
    }

    @Override
    public void onBackPressed() {

        if(selectedFragment == null || !selectedFragment.onBackPressed()) {
            //Toast.makeText(this, "test1", Toast.LENGTH_SHORT).show();
            // Selected fragment did not consume the back press event.
            title.setText(currentFrag);
            /*Intent i = new Intent(MenuItemsView.this, ClientServices.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();*/
            super.onBackPressed();
        }
    }
}
