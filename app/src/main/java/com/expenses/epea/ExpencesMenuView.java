package com.expenses.epea;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.baseArchclasses.BaseActivity;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExpencesMenuView extends BaseActivity {

    @BindView(R.id.dashboard)
    TextView dashboard;
    @BindView(R.id.expense)
    TextView expense;
    @BindView(R.id.report)
    TextView report;
    @BindView(R.id.approval)
    TextView approval;
    @BindView(R.id.profile)
    ImageButton profile;
    @BindView(R.id.logout)
    ImageButton logout;
    @BindView(R.id.cancel)
    ImageView cancel;

    SharedPreferencesKV sharedPreferencesKV;
    @BindView(R.id.user_image)
    ImageView userImage;
    @BindView(R.id.txt_userName)
    TextView txtUserName;


    AQuery aQuery;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.epea_menus);
        ButterKnife.bind(this);

        aQuery = new AQuery(ExpencesMenuView.this);
        sharedPreferencesKV = new SharedPreferencesKV(ExpencesMenuView.this);

        Log.e("client logo","logo  "+sharedPreferencesKV.getStringValue(Common.LOGO));
        aQuery.id(userImage).image(sharedPreferencesKV.getStringValue(Common.LOGO));
        txtUserName.setText(sharedPreferencesKV.getStringValue(Common.USER_NAME));

        if(sharedPreferencesKV.getStringValue(Common.APPROVAL).equals("0")){
            approval.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick({R.id.dashboard, R.id.expense, R.id.report, R.id.approval, R.id.profile, R.id.logout, R.id.cancel})
    public void onViewClicked(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.dashboard:
                i = new Intent(ExpencesMenuView.this, MenuItemsView.class);
                i.putExtra("type", "dashboard");
                startActivity(i);
                finish();
                break;
            case R.id.expense:
                i = new Intent(ExpencesMenuView.this, MenuItemsView.class);
                i.putExtra("type", "expense");
                startActivity(i);
                finish();
                break;
            case R.id.report:
                i = new Intent(ExpencesMenuView.this, MenuItemsView.class);
                i.putExtra("type", "report");
                startActivity(i);
                finish();
                break;
            case R.id.approval:
                i = new Intent(ExpencesMenuView.this, MenuItemsView.class);
                i.putExtra("type", "approval");
                startActivity(i);
                finish();
                break;
            case R.id.profile:
                i = new Intent(ExpencesMenuView.this, ProfileActivity.class);
                //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                break;
            case R.id.logout:
                checkLogout(ExpencesMenuView.this,"Are you sure?","you want to Logout from your EPEA Account","Confirm");
                break;
            case R.id.cancel:
                finish();
                break;
        }
    }

    public void checkLogout(final Context ctx , String title, String msg, final String btn_positive) {

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(btn_positive,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                            sharedPreferencesKV.removeUserData();
                            Intent i = new Intent(ExpencesMenuView.this, LoginScreen.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
