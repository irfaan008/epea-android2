package com.expenses.epea;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.androidquery.AQuery;
import com.androidquery.util.Constants;
import com.expenses.epea.appUtils.ANKProgressDialog;
import com.expenses.epea.appUtils.Common;
import com.expenses.epea.appUtils.Constant;
import com.expenses.epea.appUtils.DownloadTask;
import com.expenses.epea.appUtils.SharedPreferencesKV;
import com.expenses.epea.appUtils.Utils;
import com.expenses.epea.baseArchclasses.BaseActivity;
import com.expenses.epea.remote.VolleyResponseListener;
import com.expenses.epea.remote.VolleyUtils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/*http://www.epeaservices.co.uk/wservice/api/employee/approval/2496
        {"status":200,"message":"Get expenses for approval","data":[]}
        not valid response if not have any expense.*/
public class ExpensesDetail extends BaseActivity {

    String expenceID;

    ProgressDialog pd;
    String SERVER_URL;
    AQuery aQuery;
    String mRefID;
    BottomSheetDialog mBottomSheetDialog;
    View bottomSheet;

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.img_expense)
    ImageView imgExpense;
    @BindView(R.id.ref_no)
    TextView refNo;
    @BindView(R.id.project_code)
    TextView projectCode;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.notes)
    TextView notes;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.net_amount)
    TextView netAmount;
    @BindView(R.id.vat_amount)
    TextView vatAmount;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.approve)
    Button approve;
    @BindView(R.id.reject)
    Button reject;

    EditText person_name, reason;
    Button proceed;
    String receiptImg = "";
    String userID;
    String expenseType;
    PopupWindow popupWindow;
    String approvalStatus = "1";
    @BindView(R.id.ll_upload_receipt)
    LinearLayout llUploadReceipt;
    JSONArray receiptImages;
    SharedPreferencesKV sharedPreferencesKV;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.header_booking_no)
    TextView headerBookingNo;
    @BindView(R.id.header_project_code)
    TextView headerProjectCode;
    @BindView(R.id.header_notes)
    TextView headerNotes;
    @BindView(R.id.bookingNo)
    TextView bookingNo;
    private boolean isProjectCode;
    private  boolean isBookingNo;
    private  boolean isNotes;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expense_detail);
        ButterKnife.bind(this);

        sharedPreferencesKV = new SharedPreferencesKV(ExpensesDetail.this);
        isBookingNo = sharedPreferencesKV.getBooleanValue(Common.BOOKING_ID);
        isProjectCode = sharedPreferencesKV.getBooleanValue(Common.PROJECT);
        isNotes = sharedPreferencesKV.getBooleanValue(Common.NOTES);

        aQuery = new AQuery(ExpensesDetail.this);

        if (getIntent().getExtras() != null) {
            //Toast.makeText(this, "" + getIntent().getStringExtra("expID"), Toast.LENGTH_SHORT).show();
            expenceID = getIntent().getStringExtra("expID");
            userID = getIntent().getStringExtra("userID");
            expenseType = getIntent().getStringExtra("type");
            getExpenseDetail(ExpensesDetail.this, expenceID);
        } else {
            Toast.makeText(this, "Expense id not found", Toast.LENGTH_SHORT).show();
        }

        approve.setVisibility(View.GONE);
        reject.setVisibility(View.GONE);
        /*if(expenseType.equals("approval")){
            approve.setVisibility(View.VISIBLE);
            reject.setVisibility(View.VISIBLE);
        }else{
            approve.setVisibility(View.GONE);
            reject.setVisibility(View.GONE);
        }*/

        mBottomSheetDialog = new BottomSheetDialog(ExpensesDetail.this);

        bottomSheet = getLayoutInflater().inflate(R.layout.popup_rejected_by, null);

        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.hide();

        person_name = (EditText) bottomSheet.findViewById(R.id.person_name);
        reason = (EditText) bottomSheet.findViewById(R.id.reason);
        proceed = (Button) bottomSheet.findViewById(R.id.proceed);
        person_name.setEnabled(false);
        person_name.setText(Common.getCommonObject().getUserName(ExpensesDetail.this));

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (person_name.getText().toString().length() == 0) {
                    Toast.makeText(ExpensesDetail.this, "Invalid person name", Toast.LENGTH_SHORT).show();
                } else if (reason.getText().toString().length() == 0) {
                    Toast.makeText(ExpensesDetail.this, "Invalid Reason", Toast.LENGTH_SHORT).show();
                } else {
                    sendReject(ExpensesDetail.this, person_name.getText().toString(), reason.getText().toString());
                }
            }
        });

        imgExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //[{"receipt":"https:\/\/www.epeaservices.co.uk\/expense-claim\/beta\/120819125456802922929.pdf"}]
                Log.e("11111111", "1111111111    " + receiptImg);
                try {
                    JSONArray images = new JSONArray(receiptImg);
                    receiptImg = images.getJSONObject(0).getString("receipt");
                    getImageFileView(receiptImg);
                } catch (JSONException e) {
                    e.printStackTrace();
                    getImageFileView(receiptImg);
                }

            }
        });
    }


    private void getExpenseDetail(Context ctx, String mExpenceId) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        //String userID = Common.getCommonObject().getUserId(ExpensesDetail.this);
        SERVER_URL = Constant.SERVER_URL + "api/employee/expense/" + userID + "/expenses?id=" + mExpenceId;
        //SERVER_URL = Constant.SERVER_URL + "api/employee/expense/2490/expense?id=65";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.EXPENSE_DETAIL);
    }


    private void sendServerRequest(final Context ctx, final String url, final int responseId) {
        VolleyUtils.makeJsonGETReq(ctx, Request.Method.POST, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(menu, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.EXPENSE_DETAIL) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            JSONObject expenseDetail = jsonRootObject.getJSONArray("data").getJSONObject(0);
                            mRefID = expenseDetail.getString("reference_no");
                            approvalStatus = expenseDetail.getString("status");
                            title.setText("Expenses Claim \n#" + expenseDetail.getString("reference_no"));

                            //Toast.makeText(ctx, ""+receiptImg, Toast.LENGTH_SHORT).show();
                            receiptImages = expenseDetail.getJSONArray("receipts");
                            if (receiptImages.length() > 1) {

                                for (int i = 0; i < receiptImages.length(); i++) {
                                    addImageView(receiptImages.getJSONObject(i).getString("receipt"), i);
                                }

                            } else {
                                receiptImg = expenseDetail.getJSONArray("receipts").getJSONObject(0).getString("receipt");
                                if (receiptImg.contains(".pdf") || receiptImg.contains(".PDF")) {
                                    imgExpense.setImageResource(R.drawable.ic_pdf);
                                } else if (receiptImg.contains(".jpg") || receiptImg.contains(".JPG")
                                        || receiptImg.contains(".JPEG") || receiptImg.contains(".jpeg")
                                        || receiptImg.contains(".png") || receiptImg.contains(".PNG")) {
                                    //aQuery.id(imgExpense).image(receiptImg);
                                    Bitmap placeholder = aQuery.getCachedImage(R.drawable.otp_logo);
                                    aQuery.id(imgExpense).progress(progress).image(receiptImg, false, true, 0, 0, placeholder, Constants.FADE_IN);
                                } else if (receiptImg.toLowerCase().contains(".doc") || receiptImg.toLowerCase().contains(".docs")
                                        || receiptImg.toLowerCase().contains(".docx")) {
                                    imgExpense.setImageResource(R.drawable.ic_documents);
                                } else {
                                    imgExpense.setImageResource(R.drawable.image_not_found);
                                }
                            }


                            //aQuery.id(imgExpense).image(expenseDetail.getString("receipts"));
                            refNo.setText("#" + expenseDetail.getString("reference_no"));

                            if(isProjectCode) {
                                projectCode.setVisibility(View.VISIBLE);
                                headerProjectCode.setVisibility(View.VISIBLE);
                                projectCode.setText(expenseDetail.getString("project"));

                            } else {
                                projectCode.setVisibility(View.GONE);
                                headerProjectCode.setVisibility(View.GONE);
                            }
                            if(isNotes) {
                                notes.setVisibility(View.VISIBLE);
                                headerNotes.setVisibility(View.VISIBLE);
                                notes.setText(expenseDetail.getString("notes"));
                            } else {
                                notes.setVisibility(View.GONE);
                                headerNotes.setVisibility(View.GONE);
                            }
                            if(isBookingNo){
                                headerBookingNo.setVisibility(View.VISIBLE);
                                bookingNo.setVisibility(View.VISIBLE);
                                bookingNo.setText(expenseDetail.getString("booking_no"));
                            } else {
                                headerBookingNo.setVisibility(View.GONE);
                                bookingNo.setVisibility(View.GONE);
                            }

                            date.setText(Utils.parseDateToddMMyyyy(expenseDetail.getString("claim_date")));
                            category.setText(expenseDetail.getString("category"));

                            description.setText(expenseDetail.getString("description"));
                            netAmount.setText(Html.fromHtml(expenseDetail.getString("currency")) + " " + expenseDetail.getString("net"));
                            vatAmount.setText(Html.fromHtml(expenseDetail.getString("currency")) + " " + expenseDetail.getString("vat"));
                            //totalAmount.setText(" = " + Html.fromHtml(expenseDetail.getString("currency")) + " " + expenseDetail.getString("total"));
                            String currencyTotal = sharedPreferencesKV.getStringValue(Common.CURRENCY);
                            totalAmount.setText(currencyTotal + " " + expenseDetail.getString("total"));
                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                } else if (responseId == VolleyResponseListener.APPROVE_SINGLE_EXPENSE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            Toast.makeText(ctx, "" + jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            //finish();

                            Intent i = new Intent(ExpensesDetail.this, MenuItemsView.class);
                            i.putExtra("type", "approval");
                            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @OnClick({R.id.approve, R.id.reject, R.id.menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.approve:
                sendApprove(ExpensesDetail.this);
                break;
            case R.id.reject:
                //mBottomSheetDialog.show();
                break;
            case R.id.menu:
                finish();
                break;
        }
    }

    /*public static final int APPROVE_SINGLE_EXPENSE = 15;
    public static final int REJECT_SINGLE_EXPENSE = 16;*/
    private void sendApprove(Context ctx) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        /*Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);*/

        String userID = Common.getCommonObject().getUserId(ExpensesDetail.this);
        SERVER_URL = Constant.SERVER_URL + "api/expenses/approval/" + mRefID + "/expenses?status=" + approvalStatus;
        //SERVER_URL = Constant.SERVER_URL + "api/expenses/approval/061811194110/expenses?status=1";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    ");
        sendServerRequest(ctx, SERVER_URL, VolleyResponseListener.APPROVE_SINGLE_EXPENSE);
    }

    private void sendReject(Context ctx, String personName, String reason) {

        pd = ANKProgressDialog.showLoader(ctx, false, "Loading..");

        Map<String, String> params = new HashMap<String, String>();
        params.put("reference_no", mRefID);
        params.put("rejected_by", personName);
        params.put("reason", reason);

        SERVER_URL = Constant.SERVER_URL + "api/expenses/rejected/";

        Log.e("request  ", "request server url:-    " + SERVER_URL + "    " + params);
        sendPostServerRequest(ctx, SERVER_URL, params, VolleyResponseListener.REJECT_SINGLE_EXPENSE);
    }

    private void sendPostServerRequest(final Context ctx, final String url, final Map<String, String> parameters, final int responseId) {

        VolleyUtils.makePostReq(ctx, Request.Method.POST, parameters, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                pd.dismiss();
                showSnackBar(menu, message);
            }

            @Override
            public void onResponse(Object response, int responseId) {
                pd.dismiss();
                Log.e("response  ", "response  " + response.toString());
                if (responseId == VolleyResponseListener.REJECT_SINGLE_EXPENSE) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(response.toString());
                        if (jsonRootObject.getInt("status") == 200) {

                            Toast.makeText(ctx, "" + jsonRootObject.getString("message"), Toast.LENGTH_SHORT).show();
                            //finish();

                            Intent i = new Intent(ExpensesDetail.this, MenuItemsView.class);
                            i.putExtra("type", "approval");
                            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ctx, "" + Utils.statusMsg(ctx, jsonRootObject.getInt("status"), jsonRootObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ctx, "Something went wrong !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void addImageView(String receiptImg, int position) {
        ImageView imgReceipt = new ImageView(ExpensesDetail.this);
        //RelativeLayout relativelayout = (RelativeLayout)findViewById(R.id.relativeLayout);
        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(120, LinearLayout.LayoutParams.MATCH_PARENT);


        Log.e("111111111", "11111111    " + receiptImg);

        if (receiptImg.contains(".pdf") || receiptImg.contains(".PDF")) {
            imgReceipt.setImageResource(R.drawable.ic_pdf);
        } else if (receiptImg.contains(".jpg") || receiptImg.contains(".JPG")
                || receiptImg.contains(".JPEG") || receiptImg.contains(".jpeg")
                || receiptImg.contains(".png") || receiptImg.contains(".PNG")) {
            //aQuery.id(imgReceipt).image(receiptImg);
            Bitmap placeholder = aQuery.getCachedImage(R.drawable.image_not_found);
            aQuery.id(imgReceipt).image(receiptImg, false, true, 0, 0, placeholder, Constants.FADE_IN);
        } else if (receiptImg.toLowerCase().contains(".doc") || receiptImg.toLowerCase().contains(".docs")
                || receiptImg.toLowerCase().contains(".docx")) {
            imgReceipt.setImageResource(R.drawable.ic_documents);
        } else {
            imgReceipt.setImageResource(R.drawable.image_not_found);
        }
        imgReceipt.setTag(position);
        imgReceipt.setBackground(getResources().getDrawable(R.drawable.dotted_rectangle));
        params.setMargins(15, 0, 0, 0);
        imgReceipt.setLayoutParams(params);
        llUploadReceipt.addView(imgReceipt);

        imgReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Toast.makeText(ExpensesDetail.this, ""+imgReceipt.getTag(), Toast.LENGTH_SHORT).show();
                try {
                    getImageFileView(receiptImages.getJSONObject((int) imgReceipt.getTag()).getString("receipt"));
                } catch (JSONException e) {
                    Toast.makeText(ExpensesDetail.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        });

    }

    public void getImageFileView(String receiptImg) {
        if (receiptImg.contains(".jpg") || receiptImg.contains(".JPG")
                || receiptImg.contains(".JPEG") || receiptImg.contains(".jpeg")
                || receiptImg.contains(".png") || receiptImg.contains(".PNG")) {
            popupWindowFullImage(receiptImg);
        } else if (receiptImg.contains(".doc") || receiptImg.contains(".docs") || receiptImg.contains(".docx")
                || receiptImg.contains(".pdf")
                || receiptImg.contains(".PDF")) {
            SERVER_URL = receiptImg;

            SERVER_URL = SERVER_URL.replaceAll(" ", "%20");
            Log.e("1111111", "111111 SERVER_URL  " + SERVER_URL);

            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Log.v("GIFT CARDS ACTIVITY", "Permission granted");
                    //File write logic here
                    if (!Common.getCommonObject().isNetworkAvailable(ExpensesDetail.this)) {
                        Toast.makeText(ExpensesDetail.this, "No internet connection", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    final DownloadTask downloadTask = new DownloadTask(ExpensesDetail.this, "expense_" + expenceID);
                    downloadTask.execute(SERVER_URL);
                } else {
                    ActivityCompat.requestPermissions(ExpensesDetail.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            } else {
                if (!Common.getCommonObject().isNetworkAvailable(ExpensesDetail.this)) {
                    Toast.makeText(ExpensesDetail.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    return;
                }
                final DownloadTask downloadTask = new DownloadTask(ExpensesDetail.this, "expense_" + expenceID);
                downloadTask.execute(SERVER_URL);
            }
        } else {

            Toast.makeText(ExpensesDetail.this, "File Not Found", Toast.LENGTH_SHORT).show();
        }
    }

    public void popupWindowFullImage(String imgView) {

        final View popupView = getLayoutInflater().inflate(R.layout.popup_full_image, null);

        ImageView full_image = (ImageView) popupView.findViewById(R.id.full_image);
        ImageView img_cancel = (ImageView) popupView.findViewById(R.id.img_cancel);

        aQuery.id(full_image).image(imgView);

        popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        ///popupWindow.setAnimationStyle(R.style.PopupAnimation);

        popupWindow.getContentView().setFocusableInTouchMode(true);
        popupWindow.getContentView().setFocusable(true);
        popupWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_MENU && event.getRepeatCount() == 0
                        && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });

        popupWindow.showAtLocation(imgExpense, Gravity.CENTER, 0, 0);

    }

}
